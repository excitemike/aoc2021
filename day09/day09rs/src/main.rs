use lazy_static::*;
use plotters::prelude::*;
type R = Result<(), Box<dyn std::error::Error>>;
fn heightmap_row(row: &str) -> Vec<u8> {
    row.chars().map(|c| c.to_digit(10).unwrap() as u8).collect()
}
fn risk_level(heightmap: &[Vec<u8>], row: usize, col: usize) -> u8 {
    let nrows = heightmap.len();
    let ncols = heightmap[0].len();
    let me = heightmap[row][col];
    let up = if row > 0 {
        heightmap[row - 1][col]
    } else {
        u8::MAX
    };
    let down = if row < nrows - 1 {
        heightmap[row + 1][col]
    } else {
        u8::MAX
    };
    let left = if col > 0 {
        heightmap[row][col - 1]
    } else {
        u8::MAX
    };
    let right = if col < ncols - 1 {
        heightmap[row][col + 1]
    } else {
        u8::MAX
    };
    if [up, down, left, right].into_iter().all(|x| x > me) {
        1 + me
    } else {
        0
    }
}
fn hex_to_rgb(x: u32) -> RGBColor {
    RGBColor(
        (x >> 16).try_into().unwrap(),
        ((x >> 8) & 0xff).try_into().unwrap(),
        (x & 0xff).try_into().unwrap(),
    )
}
lazy_static! {
    static ref HEIGHT_COLORS: [RGBColor; 10] = [
        hex_to_rgb(0x000000),
        hex_to_rgb(0x000000),
        hex_to_rgb(0x252525),
        hex_to_rgb(0x525252),
        hex_to_rgb(0x737373),
        hex_to_rgb(0x969696),
        hex_to_rgb(0xbdbdbd),
        hex_to_rgb(0xd9d9d9),
        hex_to_rgb(0xf0f0f0),
        hex_to_rgb(0xffffff),
    ];
    static ref RISK_COLORS: [RGBColor; 11] = [
        hex_to_rgb(0xff3300),
        hex_to_rgb(0xff3300),
        hex_to_rgb(0xff3300),
        hex_to_rgb(0xff3300),
        hex_to_rgb(0xff3300),
        hex_to_rgb(0xff3300),
        hex_to_rgb(0xff3300),
        hex_to_rgb(0xff3300),
        hex_to_rgb(0xff3300),
        hex_to_rgb(0xff3300),
        hex_to_rgb(0xff3300),
    ];
}
fn main() -> R {
    let input = std::fs::read_to_string("../input")?;
    let lines = input.split("\r\n");
    let heightmap: Vec<Vec<u8>> = lines.map(heightmap_row).collect();
    let nrows = heightmap.len();
    let ncols = heightmap[0].len();
    let row_risk = |row| {
        (0..ncols)
            .map(|col| risk_level(&heightmap, row, col) as usize)
            .sum::<usize>()
    };
    println!("Part 1: {}", (0..nrows).map(row_risk).sum::<usize>());
    p1_render(&heightmap, &*HEIGHT_COLORS, &*RISK_COLORS)?;
    let mut stack = Vec::new();
    let mut basin_map: Vec<Vec<Option<u8>>> = Vec::from_iter((0..nrows).map(|_| vec![None; ncols]));
    for row in 0..nrows {
        for col in 0..ncols {
            if 0 != risk_level(&heightmap, row, col) {
                stack.push((row, col, stack.len()));
            }
        }
    }
    let nbasins = stack.len();
    while !stack.is_empty() {
        let (row, col, i) = stack.pop().unwrap();
        if heightmap[row][col] != 9 {
            basin_map[row][col] = Some(i.try_into()?);
            if (row > 0) && basin_map[row - 1][col].is_none() {
                stack.push((row - 1, col, i))
            }
            if (row < nrows - 1) && basin_map[row + 1][col].is_none() {
                stack.push((row + 1, col, i))
            }
            if (col > 0) && basin_map[row][col - 1].is_none() {
                stack.push((row, col - 1, i))
            }
            if (col < ncols - 1) && basin_map[row][col + 1].is_none() {
                stack.push((row, col + 1, i))
            }
        }
    }
    p2_render(&basin_map)?;
    let mut basin_sizes = vec![0usize; nbasins];
    for row in basin_map {
        for basin_index in row.into_iter().flatten() {
            basin_sizes[basin_index as usize] += 1;
        }
    }
    basin_sizes.sort_unstable();
    println!(
        "Part 2: {}",
        basin_sizes.iter().rev().take(3).product::<usize>()
    );
    Ok(())
}
fn p1_render<const N: usize, const M: usize>(
    heightmap: &[Vec<u8>],
    height_colors: &[RGBColor; N],
    risk_colors: &[RGBColor; M],
) -> R {
    let nrows = heightmap.len();
    let ncols = heightmap[0].len();
    let drawing_area = BitMapBackend::new("p1.png", (600, 600)).into_drawing_area();
    let drawing_grid = drawing_area.split_evenly((nrows, ncols));
    for (i, area) in drawing_grid.into_iter().enumerate() {
        let row = i / ncols;
        let col = i % ncols;
        let color = match risk_level(heightmap, row, col) {
            0 => &height_colors[heightmap[row][col] as usize],
            n => &risk_colors[n as usize],
        };
        area.fill(color)?;
    }
    Ok(())
}
fn p2_render(basin_map: &[Vec<Option<u8>>]) -> R {
    let nrows = basin_map.len();
    let ncols = basin_map[0].len();
    let drawing_area = BitMapBackend::new("p2.png", (600, 600)).into_drawing_area();
    let drawing_grid = drawing_area.split_evenly((nrows, ncols));
    for (i, area) in drawing_grid.into_iter().enumerate() {
        let row = i / ncols;
        let col = i % ncols;
        match basin_map[row][col] {
            Some(n) => area.fill(&Palette99::pick(n.into()))?,
            None => area.fill(&hex_to_rgb(0xcccccc))?,
        };
    }
    Ok(())
}
