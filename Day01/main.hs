import GHC.List (foldl')

mapAdjacent :: (a -> a -> b) -> [a] -> [b]
mapAdjacent f xs = zipWith f xs (tail xs)

day_1 :: IO ()
day_1 =
  do
    input <- readFile "input"
    let delta_list = deltas $ readings input
    let increase_count = foldl' fold_f 0 delta_list
    putStrLn $ "Part 1: " ++ show increase_count
  where
    readInt :: String -> Integer
    readInt = read

    readings :: String -> [Integer]
    readings input = map readInt $ lines input

    deltas :: Num a => [a] -> [a]
    deltas values = mapAdjacent (flip (-)) values

    fold_f :: Integer -> Integer -> Integer
    fold_f accum x
      | x > 0 = accum + 1
      | otherwise = accum

day_2 :: IO ()
day_2 =
  do
    input <- readFile "input"
    let triples = windows $ readings input
    let combined = map (\(a, b, c) -> a + b + c) triples
    let delta_list = deltas combined
    let increase_count = foldl' fold_f 0 delta_list
    putStrLn $ "Part 2: " ++ show increase_count
  where
    readInt :: String -> Integer
    readInt = read

    readings :: String -> [Integer]
    readings input = map readInt $ lines input

    windows :: [a] -> [(a, a, a)]
    windows xs = zip3 xs (drop 1 xs) (drop 2 xs)

    deltas :: Num a => [a] -> [a]
    deltas values = mapAdjacent (flip (-)) values

    fold_f :: Integer -> Integer -> Integer
    fold_f accum x
      | x > 0 = accum + 1
      | otherwise = accum

main :: IO ()
main =
  do
    day_1
    day_2