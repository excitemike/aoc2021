import Hs.Day18 (p1,p2)
main = do
  input <- readFile "input"
  putStr "Part 1: "
  print $ p1 input
  putStr "Part 2: "
  print $ p2 input