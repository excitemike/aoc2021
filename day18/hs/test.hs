import Control.Exception (assert)
import Control.Monad (unless)
import qualified Data.Map as Map
import Data.Maybe (fromJust)
import Hs.Day18 (add, addReduce, addReduceMany, explode, magnitude, p1, p2, parseAddReduce, readSnailNumber, reduce, split)

assertEq :: (Eq a, Show a) => a -> a -> IO ()
assertEq expected got = unless (expected == got) $ error $ "\nexpected " ++ show expected ++ "\ngot      " ++ show got

discard :: a -> IO ()
discard a = putStr $ seq a ""

main :: IO ()
main = do
  putStr "testing readSnailNumber: "
  discard $ readSnailNumber "1"
  assertEq (readSnailNumber "1") $ Map.fromList [("", 1)]
  assertEq (readSnailNumber "[1,2]") $ Map.fromList [("l", 1), ("r", 2)]
  assertEq (readSnailNumber "[[1,2],3]") $ Map.fromList [("ll", 1), ("lr", 2), ("r", 3)]
  discard $ readSnailNumber "[9,[8,7]]"
  discard $ readSnailNumber "[[1,9],[8,5]]"
  discard $ readSnailNumber "[[[[1,2],[3,4]],[[5,6],[7,8]]],9]"
  discard $ readSnailNumber "[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]"
  discard $ readSnailNumber "[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]"
  discard $ readSnailNumber "[[3,4],5]"
  discard $ readSnailNumber "[[1,2],[[3,4],5]]"
  putStrLn "ok"

  putStr "testing add: "
  let [a, b, c] = map readSnailNumber $ words "[1,2] [[3,4],5] [[1,2],[[3,4],5]]"
  assertEq c $ add a b
  putStrLn "ok"

  putStr "testing explode: "
  let got = explode $ readSnailNumber "[1,2]"
  assertEq Nothing got
  testExplode "[[[[[9,8],1],2],3],4]" "[[[[0,9],2],3],4]"
  testExplode "[7,[6,[5,[4,[3,2]]]]]" "[7,[6,[5,[7,0]]]]"
  testExplode "[[6,[5,[4,[3,2]]]],1]" "[[6,[5,[7,0]]],3]"
  testExplode "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]" "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"
  testExplode "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]" "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"
  putStrLn "ok"

  putStr "testing split: "
  let got = split $ readSnailNumber "[1,2]"
  assertEq Nothing got
  let [x1, x2] = map readSnailNumber $ words "[[[[4,3],4],4],[7,[[8,4],9]]] [1,1]"
  let x3 = add x1 x2
  assertEq (readSnailNumber "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]") x3
  let x4 = fromJust $ explode x3
  assertEq (readSnailNumber "[[[[0,7],4],[7,[[8,4],9]]],[1,1]]") x4
  let x5 = fromJust $ explode x4
  assertEq (readSnailNumber "[[[[0,7],4],[f,[0,d]]],[1,1]]") x5
  let x6 = fromJust $ split x5
  assertEq (readSnailNumber "[[[[0,7],4],[[7,8],[0,d]]],[1,1]]") x6
  let x7 = fromJust $ split x6
  assertEq (readSnailNumber "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]") x7
  let x8 = fromJust $ explode x7
  let expected = readSnailNumber "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"
  assertEq expected x8
  putStrLn "ok"

  putStr "testing reduce: "
  assertEq expected (reduce x3)
  putStrLn "ok"

  putStr "testing sum: "
  testSum "[1,1] [2,2] [3,3] [4,4]" "[[[[1,1],[2,2]],[3,3]],[4,4]]"
  testSum "[1,1] [2,2] [3,3] [4,4] [5,5]" "[[[[3,0],[5,3]],[4,4]],[5,5]]"
  testSum "[1,1] [2,2] [3,3] [4,4] [5,5] [6,6]" "[[[[5,0],[7,4]],[5,5]],[6,6]]"
  ex1 <- readFile "ex1"
  testSum ex1 "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"
  putStrLn "ok"

  putStr "testing magnitude: "
  testMagnitude "[1,9]" 21
  testMagnitude "[[9,1],[1,9]]" 129
  testMagnitude "[[1,2],[[3,4],5]]" 143
  testMagnitude "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]" 1384
  testMagnitude "[[[[1,1],[2,2]],[3,3]],[4,4]]" 445
  testMagnitude "[[[[3,0],[5,3]],[4,4]],[5,5]]" 791
  testMagnitude "[[[[5,0],[7,4]],[5,5]],[6,6]]" 1137
  testMagnitude "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]" 3488
  testMagnitude "[[[[7,8],[6,6]],[[6,0],[7,7]]],[[[7,8],[8,8]],[[7,9],[0,6]]]]" 3993
  testMagnitude "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]\n[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]" 3993
  ex2 <- readFile "ex2"
  testMagnitude ex2 4140
  putStrLn "ok"

  putStr "testing p2: "
  let a = readSnailNumber "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]"
  let b = readSnailNumber "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]"
  let c = readSnailNumber "[[[[7,8],[6,6]],[[6,0],[7,7]]],[[[7,8],[8,8]],[[7,9],[0,6]]]]"
  assertEq c $ addReduce a b
  assertEq 3993 $ p2 ex2

  putStrLn "ok"

testExplode :: String -> String -> IO ()
testExplode input expected = do
  let expected' = Just $ readSnailNumber expected
  let got = explode $ readSnailNumber input
  assertEq expected' got

testSplit :: String -> String -> IO ()
testSplit input expected = do
  let expected' = Just $ readSnailNumber expected
  let got = split $ readSnailNumber input
  assertEq expected' got

testSum :: String -> String -> IO ()
testSum input expected =
  assertEq
    (readSnailNumber expected)
    $ parseAddReduce $ unlines $ words input

testMagnitude :: String -> Int -> IO ()
testMagnitude input expected =
  assertEq
    expected
    $ p1 input
