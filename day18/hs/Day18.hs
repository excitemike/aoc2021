{-# LANGUAGE TupleSections #-}

module Hs.Day18 where

import Data.Char (digitToInt)
import Data.Foldable (find)
import Data.List (foldl1', isPrefixOf)
import qualified Data.Map.Merge.Strict as Map
import qualified Data.Map.Strict as Map
import Data.Maybe (fromJust)
import Debug.Trace (trace)
import Util (mapFst)

debug x = trace (show x) x

debugL s x = trace (s ++ show x) x

type SnailNumber = Map.Map String Int

readSnailNumber :: String -> SnailNumber
readSnailNumber s =
  let (sn, remaining) = readSN [] Map.empty s
   in if null remaining
        then sn
        else error "didn't read entire string"

readSN :: String -> SnailNumber -> String -> (SnailNumber, String)
readSN _ sn [] = (sn, [])
readSN addr sn ('[' : s) =
  let (sn', s') = readSN (addr ++ "l") sn s
      (sn'', s'') = readSN (addr ++ "r") sn' $ tail s'
   in (sn'', tail s'')
readSN addr sn (digit : rest) = (Map.insert addr (digitToInt digit) sn, rest)

add :: SnailNumber -> SnailNumber -> SnailNumber
add l r =
  let l' = mapFst ('l' :) $ Map.toList l
      r' = mapFst ('r' :) $ Map.toList r
   in Map.fromList $ l' ++ r'

addReduce :: SnailNumber -> SnailNumber -> SnailNumber
addReduce l r = reduce $ add l r

addReduceMany :: [SnailNumber] -> SnailNumber
addReduceMany [] = Map.empty
addReduceMany sns = foldl1' addReduce sns

parseAddReduce :: String -> SnailNumber
parseAddReduce s = addReduceMany $ map readSnailNumber $ lines s

magnitude :: SnailNumber -> Int
magnitude sn
  | null sn = 0
  | length sn == 1 = snd $ head $ Map.toList sn
  | otherwise = magnitude collapsed
  where
    keys = map fst $ Map.toList sn
    rightPartner key = init key ++ "r"
    hasPartner key = (last key == 'l') && Map.member (rightPartner key) sn
    collapsed = case find hasPartner keys of
      Just leftKey ->
        let rightKey = rightPartner leftKey
            destKey = init leftKey
            lVal = fromJust $ Map.lookup leftKey sn
            rVal = fromJust $ Map.lookup rightKey sn
            destVal = (3 * lVal + 2 * rVal)
            sn' = Map.delete leftKey sn
            sn'' = Map.delete rightKey sn'
         in Map.insert destKey destVal sn''
      Nothing -> sn

p1 :: String -> Int
p1 = magnitude . parseAddReduce

p2 :: String -> Int
p2 s = maximum $ map pairMag pairs
  where
    sns = map readSnailNumber $ lines s
    pairs = [(x, y) | x <- sns, y <- sns, x /= y]
    pairMag (a, b) = magnitude $ addReduce a b

leftOf :: String -> SnailNumber -> Maybe String
leftOf prefix sn = safe maximum $ filter (< prefix) $ Map.keys sn

rightOf :: String -> SnailNumber -> Maybe String
rightOf prefix sn = safe minimum $ filter (> prefix) $ Map.keys sn

explode :: SnailNumber -> Maybe SnailNumber
explode sn = do
  -- arrow here effectively means early return if none found to explode
  (found, _) <- find ((== 5) . length . fst) (Map.toList sn)
  let prefix = init found
  let leftVal = Map.lookup (prefix ++ "l") sn
  let rightVal = Map.lookup (prefix ++ "r") sn
  -- replace exploded with 0
  let filtered = filterOutPrefix prefix sn
  let zeroed = Map.insert prefix 0 filtered
  -- increase left neighbor
  let withLeft = case Map.lookup (prefix ++ "l") sn of
        Nothing -> zeroed
        Just x -> case leftOf prefix zeroed of
          Nothing -> zeroed
          Just leftKey -> Map.adjust (+ x) leftKey zeroed
  -- increase right neighbor
  let withRight = case rightVal of
        Nothing -> withLeft
        Just x -> case rightOf prefix withLeft of
          Nothing -> withLeft
          Just rightKey -> Map.adjust (+ x) rightKey withLeft
  return withRight

split :: SnailNumber -> Maybe SnailNumber
split sn = do
  -- arrow here effectively means early return if none found to split
  (prefix, val) <- find ((>= 10) . snd) (Map.toList sn)
  -- remove regular number
  let filtered = filterOutPrefix prefix sn
  -- insert pair
  let leftVal = val `div` 2
  let rightVal = val - leftVal
  let leftAddr = prefix ++ "l"
  let rightAddr = prefix ++ "r"
  return $ Map.insert leftAddr leftVal $ Map.insert rightAddr rightVal filtered

reduce :: SnailNumber -> SnailNumber
reduce sn =
  maybe
    (maybe sn reduce (split sn))
    reduce
    (explode sn)

filterOutPrefix :: String -> Map.Map String Int -> Map.Map String Int
filterOutPrefix prefix = Map.filterWithKey $ \k v -> not $ isPrefixOf prefix k

safe f [] = Nothing
safe f xs = Just $ f xs

main = do
  input <- readFile "input"
  putStr "Part 1: "
  print $ p1 input
  putStr "Part 2: "
  print $ p2 input
