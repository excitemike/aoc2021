{-# LANGUAGE ScopedTypeVariables #-}

import Data.Maybe (fromMaybe)
import qualified Data.Set as Set
import Debug.Trace (trace)

debug x = trace (show x) x

debugL s x = trace (s ++ show x) x

left :: Int
[left, right, top, bottom] = [195, 238, -67, -93]

countUnique = length . Set.unions . map (Set.unions . map Set.fromList)

p2 =
  countUnique
    [ [ [(dx, dy) | dx <- dxs t]
        | t <- ts dy
      ]
      | dy <- [bottom .. (-1 - bottom)]
    ]
  where
    ts dy = case calcT dy top of
      Nothing -> []
      Just low -> case calcT dy bottom of
        Nothing -> []
        Just high -> [ceiling low .. floor high]
    dxs t = [minDx t .. maxDx t]
    calcT dy target =
      let v :: Double = fromIntegral dy
          b = - ((2 * v) + 1)
          bSq = b * b
          c :: Double = 2 * fromIntegral target
          insideRadical = bSq - 4 * c
          radical = sqrt insideRadical
          numeratorOpts = [(- b) + radical, (- b) - radical]
          numeratorA = minimum numeratorOpts
          numeratorB = maximum numeratorOpts
          numerator = if numeratorA > 0 then numeratorA else numeratorB
          t = numerator / 2
       in if insideRadical < 0
            then Nothing
            else Just t
    maxT dy = min right $ floor $ fromMaybe 0 $ calcT dy bottom
    minT dy = maybe 0 ceiling $ calcT dy top
    minDx time =
      let x :: Double = fromIntegral left
          stopTime = ceiling ((-1 + sqrt (1 + (8 * x))) / 2)
          time' = min time stopTime
          t :: Double = fromIntegral time'
       in ceiling ((x + (((t * t) - t) / 2)) / t)
    maxDx time =
      let x :: Double = fromIntegral right
          stopTime = ceiling ((-1 + sqrt (1 + (8 * x))) / 2)
          time' = min time stopTime
          t :: Double = fromIntegral time'
       in floor ((x + (((t * t) - t) / 2)) / t)

main = do
  let v = -1 - bottom
  let p1 = (v * v) - (v * (v -1) `div` 2)
  putStr "Part 1: "
  print p1
  putStr "Part 2: "
  print p2
