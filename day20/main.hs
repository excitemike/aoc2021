{-# LANGUAGE TupleSections #-}
import qualified Data.Set as Set
import Debug.Trace (trace)
import Data.Foldable (Foldable(foldl'))

debug x = trace (show x) x

debugL s x = trace (s ++ show x) x

enumerate :: [a] -> [(Int, a)]
enumerate = zip [0..]

enhance :: Set.Set Int -> (Bool, Set.Set (Int,Int)) -> (Bool, Set.Set (Int,Int))
enhance algo (bordersLit, image) = ((0 `Set.member` algo) && not bordersLit, Set.fromList [(r, c) | r <- outputRowRange, c <- outputColRange, outputLit r c])
  where
    minRow = minimum $ map fst $ Set.toList image
    maxRow = maximum $ map fst $ Set.toList image
    minCol = minimum $ map snd $ Set.toList image
    maxCol = maximum $ map snd $ Set.toList image
    outputRowRange = [minRow-1..maxRow+1]
    outputColRange = [minCol-1..maxCol+1]
    outputCoords = [(r,c) | r<-outputRowRange, c<-outputColRange]
    sourcePixels r c = [(row,col) | row<-[r-1..r+1], col<-[c-1..c+1]]
    convolveWeights :: [Int]
    convolveWeights = [256,128,64, 32,16,8, 4,2,1]
    pixelValue :: Int -> Int -> Int
    pixelValue r c = sum $ zipWith (\src w->if inputLit src then w else 0) (sourcePixels r c) convolveWeights
    inputLit src@(r,c)
      | r < minRow = bordersLit
      | r > maxRow = bordersLit
      | c < minCol = bordersLit
      | c > maxCol = bordersLit
      | otherwise = src `Set.member` image
    outputLit r c = pixelValue r c `Set.member` algo

main = do
  input <- readFile "input"
  let inputLines = lines input
  let algo = readRow $ head inputLines
  let image = readRows $ drop 2 inputLines
  let images = map snd $ iterate (enhance algo) (False, image)
  putStrLn $ "Part 1: " ++ show (length (images !! 2))
  putStrLn $ "Part 2: " ++ show (length (images !! 50))

prettyPrint image = unlines $ map prettyPrintRow rowRange
  where
    minRow = (+ (-1)) $ minimum $ map fst $ Set.toList image
    maxRow = (+ 1) $ maximum $ map fst $ Set.toList image
    minCol = (+ (-1)) $ minimum $ map snd $ Set.toList image
    maxCol = (+ 1) $ maximum $ map snd $ Set.toList image
    rowRange = [minRow..maxRow]
    colRange = [minCol..maxCol]
    prettyPrintRow row = foldl' (foldF row) "" $ reverse colRange
    foldF row str column = char row column : ' ' : str
    char r c = if (r,c) `Set.member` image then '#' else '.'

readRow row = Set.fromList $ map fst $ filter ((=='#') . snd) $ enumerate row
readRows rows = Set.unions $ map convert $ enumerate $ map readRow rows
  where
    convert (row, set) = Set.map (row,) set