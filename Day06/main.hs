main = do
  input <- readFile "input"
  let ages = map read $ split ',' input
      hgram = [length $ filter (== i) ages | i <- [0 .. 9]]
      (p1, day) = iterate robsWay (hgram, 0) !! 80
      (p2, _) = iterate robsWay (p1, day) !! (256 - 80)
  print $ map sum [p1, p2]
  where
    split delimiter s = case break (== delimiter) s of
      (tok, _ : rest) -> tok : split delimiter rest
      (tok, _) -> [tok]
    robsWay (fish, day) =
      ( [ (fish !! i) + births fish day i
          | i <- [0 .. 9]
        ],
        day + 1
      )
    births fish day i = if i == mod (7 + day) 9 then fish !! mod (i + 2) 9 else 0