from collections import Counter
with open("input", "r") as f:
    state = Counter(map(int, f.read().strip().split(',')))
state = [state[i] for i in range(9)]

def sim_step(old_state):
    new_state = [old_state[i+1] if (i+1)<len(old_state) else 0 for i in range(9)]
    new_state[6] += old_state[0]
    new_state[8] = old_state[0]
    return new_state

plot_data = []

for i in range(80):
    state = sim_step(state)
    plot_data.append(sum(state))
print(f"Part 1: {plot_data[-1]}")
for i in range(256-80):
    state = sim_step(state)
    plot_data.append(sum(state))
print(f"Part 2: {plot_data[-1]}")

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import math
#plt.xkcd()
n = 256
chunk_size = 1
fig, ax = plt.subplots()
ax.set_xlabel('Days')
ax.set_ylabel('Fish')
line, = ax.plot(range(0,n),[0 for _ in range(0,n)],lw=2)
#plt.plot(plot_data)
#plt.show()
def animate(i):
    i = min(n, i*chunk_size)
    line.set_data(range(0,i), plot_data[:i])
    ax.set_ylim(0, plot_data[max(0,i-1)])
anim = FuncAnimation(fig, animate, frames=1+math.ceil(n/chunk_size), interval=33)
anim.save('anim.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
plt.show()