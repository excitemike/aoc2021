fn make_hgram<T: Iterator<Item = usize>>(data: T) -> [usize; 9] {
    let mut hgram = [0; 9];
    for i in data {
        hgram[i] += 1;
    }
    hgram
}
fn step(prev: [usize; 9]) -> [usize; 9] {
    let mut next = [0; 9];
    let zeroes = prev[0];
    for i in 0..8 {
        next[i] = prev[i + 1];
    }
    next[8] = zeroes;
    next[6] += zeroes;
    next
}
fn main() {
    let mut state = make_hgram(
        std::fs::read_to_string("input")
            .unwrap()
            .split(',')
            .map(|x| x.parse().unwrap()),
    );
    for _ in 0..80 {
        state = step(state);
    }
    println!("Part 1: {}", state.iter().sum::<usize>());
    for _ in 80..256 {
        state = step(state);
    }
    println!("Part 2: {}", state.iter().sum::<usize>());
}
