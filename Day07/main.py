with open("input", "r") as f:
    crabs = list(map(int, f.read().strip().split(',')))
def p1_cost(crab, dst):
    return abs(crab - dst)
def p2_cost(crab, dst):
    x=abs(crab - dst)
    return round( x*(x+1)/2 )
xs = list(range(min(crabs), max(crabs)))
make_ys = lambda cost_f: [sum((cost_f(crab, n) for crab in crabs)) for n in xs]
ys = list(map(make_ys, [p1_cost, p2_cost]))
mins = [min(x) for x in ys]
print(f"Part 1: {mins[0]}")
print(f"Part 2: {mins[1]}")

import matplotlib.pyplot as plt
import matplotlib.lines as lines
dsts = [next((i for i,v in enumerate(ys) if v==m)) for ys,m in zip(ys,mins)]

if False:
    plt.xkcd()
    fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(nrows=2,ncols=2)
    ax0.plot(xs, ys[0])
    ax2.plot(xs, ys[1],lw=2)
    # zoom in on p1
    start, end = dsts[0]-5, dsts[0]+5
    ax1.plot(xs[start:end], ys[0][dsts[0]-5:dsts[0]+5])
    # zoom in on p2
    start, end = dsts[1]-5, dsts[1]+5
    ax3.plot(xs[start:end], ys[1][start:end], lw=2)
    plt.show()
else:
    from collections import Counter
    hist = Counter(crabs)
    plt.xkcd()
    fig, ax0 = plt.subplots()
    maxes = [max(data) for data in ys]
    
    height = 218
    ax0.set_xlabel("horizontal position")

    hist = ax0.hist(crabs, bins=20)
    ax0.add_line(lines.Line2D([dsts[0], dsts[0]], [-5, 5], axes=ax0, color='black', label='part 1 cost'))
    ax0.add_line(lines.Line2D([dsts[1], dsts[1]], [-5, 5], color='red', axes=ax0, label='part 2 cost'))

    scaled = [[height*(datum-lo)/(hi-lo) for datum in data] for hi,lo,data in zip(maxes,mins,ys)]
    ax0.plot(xs, scaled[0], color='black')
    ax0.plot(xs, scaled[1], color='red')
    
    box = ax0.boxplot([crabs], positions=[height*0.60], vert=False, widths=height/16, meanline=False, patch_artist=True)
    for kw in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(box[kw], color='blue')
    for patch in box['boxes']:
        patch.set(facecolor='cyan')

    ax0.legend()

    ax0.set_ylabel(None)
    ax0.set_yticklabels([])
    ax0.set_ylim((-5,height+5))

    plt.show()