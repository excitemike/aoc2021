{-main = do
  input <- readFile "input"
  let crabs = read ('[' : input ++ "]") :: [Integer]
      dist target x = abs $ x - target
      dist2 target x = let d = dist target x in div (d * (d + 1)) 2
      totalCost f target = sum $ map (f target) crabs
      getCost f = minimum $ map (totalCost f) [0 .. toInteger $ length crabs]
  print $ map getCost [dist, dist2]-}
main=do{i<-readFile "input";let{c=read('[':i++"]")::[Integer];d t x=abs$x-t;e t x=let s=d t x in div(s*s+s)2;u f t=sum$map(f t)c;g f=minimum$map(u f)[0..toInteger$length c]};print$map g [d,e]}