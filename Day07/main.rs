fn main() {
    let crabs: Vec<_> = std::fs::read_to_string("input")
        .unwrap()
        .split(',')
        .map(|x| x.parse().unwrap())
        .collect();
    let dist_p1 = |dst, pos| if pos > dst { pos - dst } else { dst - pos };
    let triangular_number = |n| n * (n + 1) / 2;
    let dist_p2 = |dst, pos| triangular_number(dist_p1(dst, pos));
    fn min_cost<F: Fn(usize, usize) -> usize>(crabs: &[usize], f: F) -> usize {
        (0..crabs.len())
            .map(|dst| crabs.iter().map(|pos| f(dst, *pos)).sum())
            .min()
            .unwrap()
    }
    println!(
        "{} {}",
        min_cost(&crabs[..], dist_p1),
        min_cost(&crabs[..], dist_p2),
    )
}
