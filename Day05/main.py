from math import sqrt

sign = lambda x: 1 if x>0 else -1 if x<0 else 0

def points_in_line(line):
    ((x0,y0),(x1,y1)) = line
    xstep = sign(x1-x0)
    ystep = sign(y1-y0)
    x = x0
    y = y0
    while (x != x1) or (y != y1):
        yield (x,y)
        x += xstep
        y += ystep
    yield (x,y)

with open("input", "r") as f:
    lines = list(f.read().strip().splitlines())

data = [ [ [int(coord) for coord in pair.split(',')
           ]
           for pair in line.split(' -> ')
         ]
         for line in lines
       ]
p1_counts = [[0 for x in range(0,1000)] for y in range(0,1000)]
p1_xs = []
p1_ys = []
for line in data:
    if line[0][0]==line[1][0] or line[0][1]==line[1][1]:
        for x,y in points_in_line(line):
            if p1_counts[y][x] == 1:
                p1_xs.append(x)
                p1_ys.append(y)
            p1_counts[y][x] += 1
print("Part 1:", len(p1_xs))

p2_counts = [[0 for x in range(0,1000)] for y in range(0,1000)]
p2_xs = []
p2_ys = []
for line in data:
    for x,y in points_in_line(line):
        if p2_counts[y][x] == 1:
            p2_xs.append(x)
            p2_ys.append(y)
        p2_counts[y][x] += 1
print("Part 2:", len(p2_xs))

import matplotlib.pyplot as plt
plt.xkcd()
fig, (left, right) = plt.subplots(ncols=2, sharex=True, sharey=True)
plt.xlim(0,1000)
plt.ylim(0,1000)
for ((x0,y0),(x1,y1)) in data:
    left.plot([x0,x1],[y0,y1],linewidth=1, c='blue')
right.scatter(p2_xs,p2_ys, s=15)
right.scatter(p1_xs,p1_ys, s=15)
plt.show()