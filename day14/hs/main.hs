import Data.List (foldl')
import Data.List.Split (splitOn)
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)

type LetterHist = Map.Map Char Int

type PairHist = Map.Map (Char, Char) Int

type Rules = Map.Map (Char, Char) Char

applyRule :: Rules -> (Char, Char) -> Char
applyRule rules k =
  fromMaybe (error "no rule for pair") $
    Map.lookup k rules

frequencies = foldr (\x m -> Map.insertWith (+) x 1 m) Map.empty

getRules :: String -> Rules
getRules input = Map.fromList $ map f $ drop 2 $ lines input
  where
    f line =
      let [from, to] = splitOn " -> " line
          [a, b] = from
       in ((a, b), head to)

letterHist :: PairHist -> LetterHist
letterHist = Map.map mapF . Map.foldrWithKey foldF Map.empty
  where
    -- Why the "(x+1)/2"? everything is counted twice except the end points
    -- Adding 1 means now only the ones that weren't doubled are odd.
    -- So then they'll all correctly go to the number of occurrences when we halve it.
    mapF x = (x + 1) `div` 2
    foldF (a, b) n m = Map.insertWith (+) b n $ Map.insertWith (+) a n m

main :: IO ()
main = do
  exampleInput <- readFile "exampleinput"
  input <- readFile "input"
  putStrLn $ (++) "Example: " $ show $ run 10 exampleInput
  putStrLn $ (++) "Part 1 : " $ show $ run 10 input
  putStrLn $ (++) "Example: " $ show $ run 40 exampleInput
  putStrLn $ (++) "Part 2 : " $ show $ run 40 input

pairs :: [a] -> [(a, a)]
pairs s = zip s $ tail s

run :: Int -> [Char] -> Int
run n input = score finalLetterHist
  where
    starterStr = head $ lines input
    starterPairHist = frequencies $ pairs starterStr
    finalPairHist = (!! n) $ iterate (step rules) starterPairHist
    finalLetterHist = letterHist finalPairHist
    rules = getRules input

score :: LetterHist -> Int
score hist =
  let vs = map snd $ Map.toList hist
   in maximum vs - minimum vs

step :: Rules -> PairHist -> PairHist
step rules = Map.foldrWithKey f Map.empty
  where
    f :: (Char, Char) -> Int -> PairHist -> PairHist
    f (a, b) n m =
      let c = applyRule rules (a, b)
       in Map.insertWith (+) (a, c) n $ Map.insertWith (+) (c, b) n m
