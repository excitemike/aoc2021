import Data.Foldable (Foldable(foldl'))
import Debug.Trace (trace)
import qualified Data.Map.Strict as Map
import Data.List (partition)

debug x = trace (show x) x

debugL s x = trace (s ++ show x) x

-- example
-- p1Start = 4
-- p2Start = 8

-- real
p1Start = 7
p2Start = 5

stopAt = 21

spacesMoved rollCount = ((3 * rollCount) + 6) `mod` 10
newSpace oldSpace rollCount = ((oldSpace - 1 + spacesMoved rollCount) `mod` 10) + 1

main = do
  putStr "Part 1:"
  print $ part1LoserScore * rollCount
  putStr "Part 2:"
  print part2WinnerScore
  where
    (part1P1Score, part1P2Score, rollCount) = p1Turn p1Start p2Start 0 0 0
    part1LoserScore = min part1P1Score part1P2Score
    initialUniverseCounts = Map.singleton (p1Start, p2Start, 0, 0) 1
    (part2P1Wins, part2P2Wins) = diracP1TurnAllUniverses initialUniverseCounts 0 0
    part2WinnerScore = max part2P1Wins part2P2Wins

p1Turn :: Int -> Int -> Int -> Int -> Int -> (Int, Int, Int)
p1Turn p1Space p2Space p1Score p2Score rollCount = do
    let p1Space' = newSpace p1Space rollCount
    let p1Score' = p1Score + p1Space'
    let rollCount' = rollCount + 3
    if p1Score' >= 1000
    then (p1Score', p2Score, rollCount')
    else p2Turn p1Space' p2Space p1Score' p2Score rollCount'

p2Turn :: Int -> Int -> Int -> Int -> Int -> (Int, Int, Int)
p2Turn p1Space p2Space p1Score p2Score rollCount = do
    let p2Space' = newSpace p2Space rollCount
    let p2Score' = p2Score + p2Space'
    let rollCount' = rollCount + 3
    if p2Score' >= 1000
    then (p1Score, p2Score', rollCount')
    else p1Turn p1Space p2Space' p1Score p2Score' rollCount'

type Universe = (Int, Int, Int, Int)

diracP1TurnAllUniverses :: Map.Map Universe Int -> Int -> Int -> (Int, Int)
diracP1TurnAllUniverses unfinishedUniverses p1Wins p2Wins =
    if null unfinishedUniverses
    then (p1Wins, p2Wins)
    else diracP2TurnAllUniverses unfinishedUniverses' p1Wins' p2Wins
  where
    results = concatMap (uncurry diracP1TurnAllRolls) $ Map.toList unfinishedUniverses
    isDone ((_,_,s1,s2), _) = stopAt <= max s1 s2
    (done, notDone) = partition isDone results
    foldF :: Int -> (Universe, Int) -> Int
    foldF soFar ((_,_,s1,s2),n) = if s1>s2 then soFar+n else soFar
    p1Wins' = p1Wins + foldl' foldF 0 done
    unfinishedUniverses' = tally Map.empty notDone

diracP1TurnAllRolls :: Universe -> Int -> [(Universe, Int)]
diracP1TurnAllRolls universe count =
    [threes, fours, fives, sixes, sevens, eights, nines]
  where
    threes = diracP1Turn 3 universe count
    fours  = diracP1Turn 4 universe (count * 3)
    fives  = diracP1Turn 5 universe (count * 6)
    sixes  = diracP1Turn 6 universe (count * 7)
    sevens = diracP1Turn 7 universe (count * 6)
    eights = diracP1Turn 8 universe (count * 3)
    nines  = diracP1Turn 9 universe count

diracP1Turn :: Int -> Universe -> Int -> (Universe, Int)
diracP1Turn roll (p1Space, p2Space, p1Score, p2Score) count = (universe, count)
  where
    p1Space' = ((p1Space - 1 + roll) `mod` 10) + 1
    p1Score' = p1Score + p1Space'
    universe = (p1Space', p2Space, p1Score', p2Score)

diracP2TurnAllUniverses :: Map.Map Universe Int -> Int -> Int -> (Int, Int)
diracP2TurnAllUniverses unfinishedUniverses p1Wins p2Wins =
    if null unfinishedUniverses
    then (p1Wins, p2Wins)
    else diracP1TurnAllUniverses unfinishedUniverses' p1Wins p2Wins'
  where
    results = concatMap (uncurry diracP2TurnAllRolls) $ Map.toList unfinishedUniverses
    isDone ((_,_,s1,s2), _) = stopAt <= max s1 s2
    (done, notDone) = partition isDone results
    foldF :: Int -> (Universe, Int) -> Int
    foldF soFar ((_,_,s1,s2),n) = if s2>s1 then soFar+n else soFar
    p2Wins' = p2Wins + foldl' foldF 0 done
    unfinishedUniverses' = tally Map.empty notDone

diracP2TurnAllRolls :: Universe -> Int -> [(Universe, Int)]
diracP2TurnAllRolls universe count =
    [threes, fours, fives, sixes, sevens, eights, nines]
  where
    threes = diracP2Turn 3 universe count
    fours  = diracP2Turn 4 universe (count * 3)
    fives  = diracP2Turn 5 universe (count * 6)
    sixes  = diracP2Turn 6 universe (count * 7)
    sevens = diracP2Turn 7 universe (count * 6)
    eights = diracP2Turn 8 universe (count * 3)
    nines  = diracP2Turn 9 universe count

diracP2Turn :: Int -> Universe -> Int -> (Universe, Int)
diracP2Turn roll (p1Space, p2Space, p1Score, p2Score) count = (universe, count)
  where
    p2Space' = ((p2Space - 1 + roll) `mod` 10) + 1
    p2Score' = p2Score + p2Space'
    universe = (p1Space, p2Space', p1Score, p2Score')

tally :: Ord k => Map.Map k Int -> [(k, Int)] -> Map.Map k Int
tally = foldl' $ \m (k, n) -> Map.alter (updater n) k m
  where
    updater n Nothing = Just n
    updater n (Just x) = Just $ x + n