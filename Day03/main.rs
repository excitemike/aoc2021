fn check_nth_char(s:&str, n:usize, c:char) -> bool {
    s.chars().nth(n).as_ref().map(|x| x == &c).unwrap_or(false)
}
fn most_common_in_pos(pos: usize, entries: &[&str]) -> char {
    if entries
        .iter()
        .filter(|s|check_nth_char(s,pos,'1'))
        .count()
        * 2
        >= entries.len()
    {
        '1'
    } else {
        '0'
    }
}
fn least_common_in_pos(pos: usize, entries: &[&str]) -> char {
    match most_common_in_pos(pos, entries) {
        '1' => '0',
        _ => '1',
    }
}
fn select_entry<'a>(entries: &'a [&str], flip: bool) -> &'a str {
    let bits_per_entry = entries.first().unwrap().len();
    let mut read_buf: Vec<&str> = entries.iter().cloned().collect();
    let mut write_buf: Vec<&str> = Vec::with_capacity(read_buf.len());
    for i in 0..bits_per_entry {
        let f = if flip {
            least_common_in_pos
        } else {
            most_common_in_pos
        };
        let desired = f(i, &read_buf[..]);
        write_buf.clear();
        let new_entries = read_buf
            .iter()
            .filter(|x| x.chars().nth(i).unwrap_or('*') == desired);
        write_buf.extend(new_entries);
        if write_buf.len() == 1 {
            return write_buf.first().unwrap();
        }
        std::mem::swap(&mut read_buf, &mut write_buf);
    }
    panic!();
}
fn decode_binary(s: &str) -> usize {
    let mut x = 0;
    for c in s.chars() {
        x = 2 * x + if c == '1' { 1 } else { 0 };
    }
    x
}

fn main() {
    let input = std::fs::read_to_string("input").unwrap();
    let entries: Vec<&str> = input.trim_end().split('\n').map(str::trim_end).collect();
    let bits_per_entry = entries.first().unwrap().len();
    let gamma = (0..bits_per_entry)
        .map(|i| most_common_in_pos(i, entries.as_slice()))
        .collect::<String>();
    let gamma = decode_binary(&*gamma);
    let epsilon = (1 << bits_per_entry) - 1 - gamma;
    let oxy = select_entry(&entries, false);
    let oxy = decode_binary(&*oxy);
    let scrubber = select_entry(&entries, true);
    let scrubber = decode_binary(&*scrubber);
    println!("Part 1: {}", gamma * epsilon);
    println!("Part 2: {}", oxy * scrubber);
}
