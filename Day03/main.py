with open("input", "r") as f:
    lines = list(f.read().strip().splitlines())
bits_per_entry = len(lines[0])

def most_common_in_position(position, search_set):
    n = sum((int(line[position]) for line in search_set))
    return '1' if n >= len(search_set) / 2 else '0'
flip_dict = {'0':'1','1':'0'}
def select_entry(flip):
    result = ""
    entries = lines
    for i in range(bits_per_entry):
        desired = most_common_in_position(i, entries)
        desired = flip_dict[desired] if flip else desired
        entries = list(filter(lambda x: x[i]==desired, entries))
        if len(entries) == 1:
            return entries[0]
    return None

γ = "".join((most_common_in_position(i, lines) for i in range(bits_per_entry)))
γ = int(γ, 2)
oxy = select_entry(False)
scrubber = select_entry(True)
ε = (2**bits_per_entry - 1 - γ)
print("Part 1:", γ * ε)
print("Part 2:", int(oxy, 2) * int(scrubber, 2))