{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
import GHC.List (foldl')

input = lines <$> readFile "input"

floatLength :: [a] -> Double
floatLength = fromIntegral . length

binaryToInt :: String -> Int
binaryToInt = foldl' (\ init character -> 2 * init + read [character]) 0

mostCommonHead :: [String] -> Char
mostCommonHead entries =
    let threshold = floatLength entries / 2.0
        n_ones = floatLength $ filter ((=='1') . head) entries
    in if n_ones >= threshold
       then '1'
       else '0'
invert '0' = '1'
invert '1' = '0'
leastCommonHead :: [String] -> Char
leastCommonHead = invert .mostCommonHead

mostCommonBits entries = reverse $ mostCommonBits_ [] entries
mostCommonBits_ so_far entries
    | null (head entries) = so_far
    | otherwise =
        let x = mostCommonHead entries
            entries' = map tail entries
        in mostCommonBits_ (x:so_far) entries'

selectEntry :: ([String] -> Char) -> String -> [String] -> String
selectEntry _ prefix [] = prefix
selectEntry _ prefix [x] = prefix ++ x
selectEntry desired_f prefix entries =
    let desired = desired_f entries
        fold_f' = fold_f desired
        entries' = foldl' fold_f' [] entries
        prefix' = prefix ++ [desired]
    in selectEntry desired_f prefix' entries'
  where
    fold_f desired results (first:rest)
        | first == desired = rest:results
        | otherwise = results

main =
  do
    entries <- input
    let num_bits = length $ head entries
    let gamma = binaryToInt $ mostCommonBits entries
    let epsilon = 2 ^ num_bits - 1 - gamma
    putStrLn $ "Part 1: " ++ show (gamma * epsilon)
    let oxy = binaryToInt $ selectEntry mostCommonHead [] entries
    let scrubber = binaryToInt $ selectEntry leastCommonHead [] entries
    putStrLn $ "Part 2: " ++ show (oxy * scrubber)