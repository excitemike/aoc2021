use std::collections::{btree_map::Entry::*, BTreeMap};
type R = Result<(), Box<dyn std::error::Error>>;
fn link<'a>(graph: &mut BTreeMap<&'a str, Vec<&'a str>>, a: &'a str, b: &'a str) {
    match graph.entry(a) {
        Vacant(e) => {
            e.insert(vec![b]);
        }
        Occupied(mut e) => {
            e.get_mut().push(b);
        }
    }
}

fn p1<'a>(graph: &BTreeMap<&'a str, Vec<&'a str>>) {
    let mut n = 0;
    let mut stack = vec![(String::new(), "start")];
    while let Some((path, cave)) = stack.pop() {
        let path = format!("{},{}", path, cave);
        match cave {
            "end" => {
                n += 1;
                //println!("{}: {}", n, path);
            }
            _ => {
                for &cave in graph.get(cave).unwrap() {
                    if cave.chars().any(char::is_uppercase) || !path.contains(cave) {
                        stack.push((path.clone(), cave));
                    }
                }
            }
        }
    }
    println!("{}", n);
}

fn p2<'a>(graph: &BTreeMap<&'a str, Vec<&'a str>>) {
    let mut n = 0;
    let mut stack = vec![(String::new(), "start", true)];
    while let Some((path, cave, allow_repeat)) = stack.pop() {
        let path = format!("{} {}", path, cave);
        for &cave in graph.get(cave).unwrap() {
            match cave {
                "start" => (),
                "end" => {
                    n += 1;
                    //println!("{}:{},end", n, path);
                }
                _ if cave.chars().any(char::is_uppercase) => {
                    stack.push((path.clone(), cave, allow_repeat))
                }
                _ if path.matches(cave).count() < 1 => {
                    stack.push((path.clone(), cave, allow_repeat))
                }
                _ if allow_repeat && (path.matches(cave).count() < 2) => {
                    stack.push((path.clone(), cave, false))
                }
                _ => (),
            }
        }
    }
    println!("{}", n);
}

fn main() -> R {
    let mut graph = BTreeMap::new();
    let input = std::fs::read_to_string("input")?;
    for line in input.split("\r\n") {
        let mut i = line.split('-');
        let a = i.next().unwrap();
        let b = i.next().unwrap();
        link(&mut graph, a, b);
        link(&mut graph, b, a);
    }
    p1(&graph);
    p2(&graph);
    Ok(())
}
