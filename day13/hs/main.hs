import Data.List (intercalate, isPrefixOf)
import Data.List.Split (splitOn)
import Data.Set (Set, fromList, insert, toList)
import qualified Data.Set as Set

type Dot = (Int, Int)

type FoldF = (Int, Int) -> (Int, Int)

type ParseState = (Set Dot, [FoldF])

day13 input = "Part 1:" ++ show p1 ++ "\nPart 2:\n" ++ p2 ++ "\n" ++ fancyStr
  where
    (dots, fold : folds) = parseInput (Set.empty, []) $ lines input
    dots' = Set.map fold dots
    p1 = length dots'
    dots'' = doFolds folds dots'
    minX = minimum $ Set.map fst dots''
    maxX = maximum $ Set.map fst dots''
    minY = minimum $ Set.map snd dots''
    maxY = maximum $ Set.map snd dots''
    moveDot (x, y) = (x - minX, y - minY)
    dots''' = Set.map moveDot dots''
    tileStr row column = tile (column, row) dots'''
    rowToStr row = concatMap (tileStr row) [minX .. maxX]
    p2 = unlines $ map rowToStr [minY .. maxY]
    -- fancy
    neighbors row col = [(row-1, col), (row + 1, col), (row, col -1), (row, col + 1)]
    marSquNumber row col dots = (if Set.member (col-1, row) dots then 1 else 0)
                                + (if Set.member (col, row) dots then 2 else 0)
                                + (if Set.member (col, row-1) dots then 4 else 0)
                                + (if Set.member (col-1, row-1) dots then 8 else 0)
    {-marSquTiles =
        ["      "   -- case 0
        ,"   \\  "  -- 1
        ,"  _ /#"   -- 2
        ,"___###"   -- 3
        ,"\\## ``"  -- 4
        ,"/###/`"   -- 5
        ,"|##|##"   -- 6
        ,"/#####"   -- 7
        ,"/     "   -- 8
        ,"#| #| "   -- 9
        ,"#\\_\\##" -- 10
        ,"#\\_###"  -- 11
        ,"###```"   -- 12
        ,"####/`"   -- 13
        ,"###\\##"  -- 14
        ,"######"   -- 15
        ]
    charsPerHalf = 3
    -}
    marSquTiles =
        ["    "   -- case 0
        ,"  \\ "  -- 1
        ,"   /"   -- 2
        ,"__  "   -- 3
        ," \\  "  -- 4
        ,"/  /"   -- 5
        ," | |"   -- 6
        ,"/   "   -- 7
        ,"/   "   -- 8
        ,"| | "   -- 9
        ," \\\\ " -- 10
        ," \\  "  -- 11
        ,"__  "   -- 12
        ,"   /"   -- 13
        ,"  \\ "  -- 14
        ,"####"   -- 15
        ]
    charsPerHalf = 2
    marSquTile row col = marSquTiles !! marSquNumber row col dots'''
    topHalf row col = take charsPerHalf $ marSquTile row col
    bottomHalf row col = drop charsPerHalf $ marSquTile row col
    topHalfRow row = concatMap (topHalf row) [minX .. maxX + 1]
    bottomHalfRow row = concatMap (bottomHalf row) [minX .. maxX + 1]
    rowStr row = unlines [topHalfRow row, bottomHalfRow row]
    fancyStr = concatMap rowStr [minY .. maxY + 1]

doFolds :: [FoldF] -> Set Dot -> Set Dot
doFolds fs dots = foldl (flip Set.map) dots fs

parseInput :: ParseState -> [String] -> ParseState
parseInput (dots, folds) [] = (dots, reverse folds)
parseInput (dots, folds) (l : ls)
  | null l = parseInput (dots, folds) ls
  | "fold along " `isPrefixOf` l =
    let fold = parseFold $ drop 11 l
     in parseInput (dots, fold : folds) ls
  | otherwise =
    let dot = parseDot l
     in parseInput (insert dot dots, folds) ls

parseDot :: String -> Dot
parseDot l = case map read $ splitOn "," l of
  [a, b] -> (a, b)
  _ -> error "error in parseDot"

parseFold :: String -> FoldF
parseFold l = case head l of
  'x' -> xFold $ read $ drop 2 l
  _ -> yFold $ read $ drop 2 l

main = do
  input <- readFile "input"
  putStrLn $ day13 input

tile x dotSet
  | Set.member x dotSet = "[]"
  | otherwise = "  "

xFold n (x, y)
  | x > n = (2 * n - x, y)
  | otherwise = (x, y)

yFold n (x, y)
  | y > n = (x, 2 * n - y)
  | otherwise = (x, y)
