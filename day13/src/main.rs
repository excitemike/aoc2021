use std::collections::BTreeSet;
type DotSet = BTreeSet<(isize, isize)>;
enum Fold {
    X(isize),
    Y(isize),
}
fn fold_left(dots: DotSet, fold_point: isize) -> DotSet {
    dots.iter()
        .map(|(x, y)| {
            if x > &fold_point {
                (2 * fold_point - x, *y)
            } else {
                (*x, *y)
            }
        })
        .collect()
}
fn fold_up(dots: DotSet, fold_point: isize) -> DotSet {
    dots.iter()
        .map(|(x, y)| {
            if y > &fold_point {
                (*x, 2 * fold_point - y)
            } else {
                (*x, *y)
            }
        })
        .collect()
}
fn fold(dots: DotSet, fold: Fold) -> DotSet {
    match fold {
        Fold::X(fold_point) => fold_left(dots, fold_point),
        Fold::Y(fold_point) => fold_up(dots, fold_point),
    }
}
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input = std::fs::read_to_string("input")?;
    let mut dots = DotSet::new();
    let mut folds = Vec::new();
    for line in input.split("\r\n") {
        if line.starts_with("fold") {
            let mut i = line[11..].split('=');
            let ctor = match i.next().unwrap() {
                "x" => Fold::X,
                "y" => Fold::Y,
                _ => panic!(),
            };
            folds.push(ctor(i.next().unwrap().parse::<isize>()?));
        } else if !line.is_empty() {
            let mut i = line.split(',');
            dots.insert((
                i.next().unwrap().parse::<isize>()?,
                i.next().unwrap().parse::<isize>()?,
            ));
        }
    }
    let mut i = folds.into_iter();
    let mut dots = fold(dots, i.next().unwrap());
    println!("Part 1: {}", dots.len());
    for x in i {
        dots = fold(dots, x);
    }
    println!("Part 2:");
    let mut min_x = isize::MAX;
    let mut max_x = isize::MIN;
    let mut min_y = isize::MAX;
    let mut max_y = isize::MIN;
    for (x, y) in &dots {
        min_x = isize::min(min_x, *x);
        max_x = isize::max(max_x, *x);
        min_y = isize::min(min_y, *y);
        max_y = isize::max(max_y, *y);
    }
    for col in min_y..=max_y {
        for row in min_x..=max_x {
            print!(
                "{}",
                if dots.contains(&(row, col)) {
                    "[]"
                } else {
                    "  "
                }
            );
        }
        println!();
    }
    plot(dots)
}
fn plot(dots: DotSet) -> Result<(), Box<dyn std::error::Error>> {
    use std::io::Write;
    let mut min_x = isize::MAX;
    let mut max_x = isize::MIN;
    let mut min_y = isize::MAX;
    let mut max_y = isize::MIN;
    for (x, y) in &dots {
        min_x = isize::min(min_x, *x);
        max_x = isize::max(max_x, *x);
        min_y = isize::min(min_y, *y);
        max_y = isize::max(max_y, *y);
    }
    let display_cols = max_x + 3 - min_x;
    let display_rows = max_y + 3 - min_y;
    let w = 1000;
    let h = w * display_rows / display_cols;
    let mut f = std::fs::File::create("plot.svg")?;
    writeln!(
        f,
        r#"<svg width="{}" height="{}" viewBox="-1 -1 {} {}" xmlns="http://www.w3.org/2000/svg">"#,
        w, h, display_cols, display_rows
    )?;
    writeln!(
        f,
        r##"<rect x="-1" y="-1" width="{}" height="{}" opacity="1" fill="#FFFF99" stroke="none"/>"##,
        display_cols, display_rows
    )?;
    for (x, y) in dots {
        writeln!(
            f,
            r##"<rect x="{}" y="{}" width="1" height="1" opacity="1" fill="#0000CC" stroke="none"/>"##,
            x - min_x,
            y - min_y
        )?;
    }
    writeln!(f, "</svg>")?;
    Ok(())
}
