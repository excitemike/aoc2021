import Data.Char (digitToInt)
import Data.List (foldl')
import Text.Printf (printf)
import Data.Bits

data OpData = A [Packet] | B [Packet]
  deriving (Eq, Show)

data PacketType
  = Sum OpData
  | Prod OpData
  | Min OpData
  | Max OpData
  | Gt OpData
  | Lt OpData
  | Equals OpData
  | Literal Integer
  deriving (Eq, Show)

type Packet = (Integer, PacketType)

type BitData = (Integer, Int)

binToInt :: String -> BitData
binToInt s = (bits, len)
  where
    bitsStr = reverse $ toBits s
    bits = foldl' (\a b -> 2 * a + fromIntegral (digitToInt b)) 0 bitsStr
    len = length bitsStr

evalComp :: (Integer -> Integer -> Bool) -> OpData -> Integer
evalComp f xs = case map evalPacket $ packets xs of
  [a, b] -> if f a b then 1 else 0
  _ -> 0

evalHex :: String -> Integer
evalHex = evalPacket . fst . readPacket . binToInt

evalPacket :: Packet -> Integer
evalPacket = evalPt . snd

evalPt :: PacketType -> Integer
evalPt (Sum xs) = sum $ map evalPacket $ packets xs
evalPt (Prod xs) = product $ map evalPacket $ packets xs
evalPt (Min xs) = minimum $ map evalPacket $ packets xs
evalPt (Max xs) = maximum $ map evalPacket $ packets xs
evalPt (Gt xs) = evalComp (>) xs
evalPt (Lt xs) = evalComp (<) xs
evalPt (Equals xs) = evalComp (==) xs
evalPt (Literal x) = x

tests :: IO ()
tests = do
  expect (0x53, 8) $ reverseBits (0xca, 8)
  expect (0xca, 8) $ reverseBits (0x53, 8)
  expect 6 $ hexToVersionSum "D2FE28"
  expect 9 $ hexToVersionSum "38006F45291200"
  expect 14 $ hexToVersionSum "EE00D40C823060"
  expect 16 $ hexToVersionSum "8A004A801A8002F478"
  expect 12 $ hexToVersionSum "620080001611562C8802118E34"
  expect 23 $ hexToVersionSum "C0015000016115A2E0802F182340"
  expect 31 $ hexToVersionSum "A0016C880162017C3686B18A3D4780"
  expect 2021 $ evalHex "D2FE28"
  expect 1 $ evalHex "38006F45291200"
  expect 3 $ evalHex "EE00D40C823060"
  expect 15 $ evalHex "8A004A801A8002F478"
  expect 3 $ evalHex "C200B40A82"
  expect 54 $ evalHex "04005AC33890"
  expect 7 $ evalHex "880086C3E88112"
  expect 9 $ evalHex "CE00C43D881120"
  expect 1 $ evalHex "D8005AC2A8F0"
  expect 0 $ evalHex "F600BC2D8F"
  expect 0 $ evalHex "9C005AC2F8F0"
  expect 1 $ evalHex "9C0141080250320F1802104A08"

  expect "(1,Lt (A [(6,Literal 10),(2,Literal 20)]))" $ packetStr "38006F45291200"
  expect "(7,Max (B [(2,Literal 1),(4,Literal 2),(1,Literal 3)]))" $ packetStr "EE00D40C823060"
  expect "(4,Min (B [(1,Min (B [(5,Min (A [(6,Literal 15)]))]))]))" $ packetStr "8A004A801A8002F478"

  where
    expect expected got = if expected == got then return () else error $ "expected " ++ show expected ++ ", got " ++ show got
    packetStr = show . fst . readPacket . binToInt

hexToVersionSum :: String -> Integer
hexToVersionSum = versionSum . binToInt

main :: IO ()
main = do
  tests
  input <- readFile "input"
  putStr "Part 1: "
  print $ hexToVersionSum input
  putStr "Part 2: "
  print $ evalHex input

packets :: OpData -> [Packet]
packets (A ps) = ps
packets (B ps) = ps

popBit :: BitData -> (Integer, BitData)
popBit bitData = let (a, b) = splitBits 1 bitData in (fst a, b)

pushBit :: (Num a, Bits a) => a -> a -> a
pushBit bit orig = (orig `shiftL` 1) + bit

readLiteral :: BitData -> (PacketType, BitData)
readLiteral = f 0
  where
    f :: Integer -> BitData -> (PacketType, BitData)
    f x (_, 0) = (Literal $ fromIntegral x, (0, 0))
    f x bits =
        case mode of
          1 -> f x' rest
          _ -> (Literal $ fromIntegral x', rest)
      where
        (modeBits, bits') = splitBits 1 bits
        mode = fst modeBits
        (revByte, rest) = splitBits 4 bits'
        byte = reverseBits revByte
        x' = 16 * x + fromIntegral (fst byte)

readOperator :: (OpData -> PacketType) -> BitData -> (PacketType, BitData)
readOperator ctor bits = readOp mode
  where
    (modeBits, bits') = splitBits 1 bits
    mode = fst modeBits
    readOp 0 = let (revLen, bits'') = splitBits 15 bits'
                   len = fromIntegral $ fst $ reverseBits revLen
                   (subBits, bits''') = splitBits len bits''
                   (packets, _) = readAllPackets subBits
                in (ctor $ A packets, bits''')
    readOp _ = let (revLen, bits'') = splitBits 11 bits'
                   len = fromIntegral $ fst $ reverseBits revLen
                   (packets, bits''') = readNPackets len bits''
                in (ctor $ B packets, bits''')

readNPackets :: Integer -> BitData -> ([Packet], BitData)
readNPackets n bits = f [] n bits
  where
    f ps 0 bits = (reverse ps, bits)
    f ps n bits =
      let (p, bits') = readPacket bits
       in f (p : ps) (n -1) bits'

readPacket :: BitData -> (Packet, BitData)
readPacket bits = (packet, rest)
  where
    (revVersionBits, bits') = splitBits (3::Int) bits
    (ptype, rest) = readPacketType bits'
    versionBits = reverseBits revVersionBits
    packet = (fst versionBits, ptype)

readAllPackets :: BitData -> ([Packet], BitData)
readAllPackets bitData = f [] bitData
  where
    f :: [Packet] -> BitData -> ([Packet], BitData)
    f ps (0, _) = (reverse ps, (0, 0))
    f ps bitData =
      let (p, s') = readPacket bitData
       in f (p : ps) s'

readPacketType :: BitData -> (PacketType, BitData)
readPacketType bits =
    case fst typeCode of
      0 -> readOperator Sum rest
      1 -> readOperator Prod rest
      2 -> readOperator Min rest
      3 -> readOperator Max rest
      4 -> readLiteral rest
      5 -> readOperator Gt rest
      6 -> readOperator Lt rest
      7 -> readOperator Equals rest
      _ -> error $ "Unknown operator " ++ show typeCode
  where
    (revTypeCode, rest) = splitBits 3 bits
    typeCode :: BitData
    typeCode = reverseBits revTypeCode

reverseBits :: BitData -> BitData
reverseBits (bits, len) = (reverseNBits (fromIntegral len) bits, len)

reverseNBits :: Int -> Integer -> Integer
reverseNBits n bits = f n 0 masked
  where
    mask :: Int
    mask = (1 `shiftL` fromIntegral n) - 1
    masked :: Integer
    masked = fromIntegral bits .&. fromIntegral mask
    f :: Int -> Integer -> Integer -> Integer
    f 0 outBits _ = outBits
    f n outBits inBits = f (n-1) (pushBit bit outBits) rest
      where
        (bit, rest) = (inBits .&. 1, inBits `shiftR` 1)

-- keep in mind that the bits are reversed
splitBits :: Int -> BitData -> (BitData, BitData)
splitBits nBits (bits, len) = (lowBits, rest)
  where
    mask = ((1::Integer) `shiftL` nBits) - 1
    lowBits = (mask .&. fromIntegral bits, nBits)
    rest = (bits `shiftR` fromIntegral nBits, len - nBits)

toBits :: Foldable t => t Char -> String
toBits = concatMap (printf "%04b" . digitToInt)

versionSum :: BitData -> Integer
versionSum = versionSumP . fst . readPacket

versionSumP :: Packet -> Integer
versionSumP (v, pt) = v + versionSumPt pt

versionSumPt :: PacketType -> Integer
versionSumPt (Sum ot) = versionSumOt ot
versionSumPt (Prod ot) = versionSumOt ot
versionSumPt (Min ot) = versionSumOt ot
versionSumPt (Max ot) = versionSumOt ot
versionSumPt (Gt ot) = versionSumOt ot
versionSumPt (Lt ot) = versionSumOt ot
versionSumPt (Equals ot) = versionSumOt ot
versionSumPt (Literal _) = 0

versionSumOt :: OpData -> Integer
versionSumOt (A ps) = sum $ map versionSumP ps
versionSumOt (B ps) = sum $ map versionSumP ps