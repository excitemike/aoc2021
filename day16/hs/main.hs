import Control.Exception (assert)
import Control.Monad (void)
import Data.Char (digitToInt)
import Data.List (foldl')
import Debug.Trace (trace)
import Text.Printf (printf)

data OpData = A [Packet] | B [Packet]
  deriving (Eq, Show)

data PacketType
  = Sum OpData
  | Prod OpData
  | Min OpData
  | Max OpData
  | Gt OpData
  | Lt OpData
  | Equals OpData
  | Literal Integer
  | None
  deriving (Eq, Show)

type Packet = (Integer, PacketType)

binToInt :: String -> Integer
binToInt = foldl' (\a b -> 2 * a + fromIntegral (digitToInt b)) 0

evalComp :: (Integer -> Integer -> Bool) -> OpData -> Integer
evalComp f xs = case map evalPacket $ packets xs of
  [a, b] -> if f a b then 1 else 0
  _ -> 0

evalHex :: String -> Integer
evalHex = evalPacket . fst . readPacket . toBits

evalPacket :: Packet -> Integer
evalPacket = evalPt . snd

evalPt :: PacketType -> Integer
evalPt (Sum xs) = sum $ map evalPacket $ packets xs
evalPt (Prod xs) = product $ map evalPacket $ packets xs
evalPt (Min xs) = minimum $ map evalPacket $ packets xs
evalPt (Max xs) = maximum $ map evalPacket $ packets xs
evalPt (Gt xs) = evalComp (>) xs
evalPt (Lt xs) = evalComp (<) xs
evalPt (Equals xs) = evalComp (==) xs
evalPt (Literal x) = x
evalPt x = error $ "evalPt error: " ++ show x

testExamples :: IO ()
testExamples = do
  p1Test "D2FE28" "(6,Literal 2021)"
  p1Test "38006F45291200" "(1,Lt (A [(6,Literal 10),(2,Literal 20)]))"
  p1Test "EE00D40C823060" "(7,Max (B [(2,Literal 1),(4,Literal 2),(1,Literal 3)]))"
  p1Test "8A004A801A8002F478" "(4,Min (B [(1,Min (B [(5,Min (A [(6,Literal 15)]))]))]))"
  testVersionSum "8A004A801A8002F478" 16
  testVersionSum "620080001611562C8802118E34" 12
  testVersionSum "C0015000016115A2E0802F182340" 23
  testVersionSum "A0016C880162017C3686B18A3D4780" 31
  mapM_ p2Ex [("C200B40A82", 3), ("04005AC33890", 54), ("880086C3E88112", 7), ("CE00C43D881120", 9), ("D8005AC2A8F0", 1), ("F600BC2D8F", 0), ("9C005AC2F8F0", 0), ("9C0141080250320F1802104A08", 1)]
  where
    expect expected got = if expected == got then return () else error $ "expected " ++ show expected ++ ", got " ++ show got
    p1Test :: String -> String -> IO ()
    p1Test hex expected = expect expected $ show $ fst $ readPacket $ toBits hex
    testVersionSum :: String -> Integer -> IO ()
    testVersionSum s n = expect n $ versionSum s
    p2Ex :: (String, Integer) -> IO ()
    p2Ex (s, n) = expect n $ evalHex s

hexCharToBits :: Char -> String
hexCharToBits c = printf "%04b" $ digitToInt c

main :: IO ()
main = do
  testExamples
  putStr "Part 1: "
  input <- readFile "input"
  print $ versionSumP $ fst $ readPacket $ toBits input
  putStr "Part 2: "
  print $ evalHex input

packets :: OpData -> [Packet]
packets (A ps) = ps
packets (B ps) = ps

readLiteral :: String -> (PacketType, String)
readLiteral "" = (Literal 0, [])
readLiteral s = f 0 s
  where
    f :: Integer -> String -> (PacketType, String)
    f x [] = (Literal x, [])
    f x (a : b : c : d : e : rest) = case a of
      '1' -> f (x * 16 + binToInt [b, c, d, e]) rest
      _ -> (Literal $ x * 16 + binToInt [b, c, d, e], rest)
    f x s
      | all (== '0') s = (Literal x, [])
      | otherwise = error $ "error in readLiteral: " ++ show x ++ ", " ++ show s

readOperator :: (OpData -> PacketType) -> String -> (PacketType, String)
readOperator ctor ('0' : s) =
  let (lenStr, s') = splitAt 15 s
      len = fromIntegral $ binToInt lenStr
      (subStr, s'') = splitAt len s'
      (packets, _) = readAllPackets subStr
   in (ctor $ A packets, s'')
readOperator ctor ('1' : s) =
  let (lenStr, s') = splitAt 11 s
      len = binToInt lenStr
      (packets, s'') = readNPackets len s'
   in (ctor $ B packets, s'')
readOperator _ _ = error "readOperator error"

readNPackets :: Integer -> String -> ([Packet], String)
readNPackets n s = f [] n s
  where
    f ps 0 s = (reverse ps, s)
    f ps n s =
      let (p, s') = readPacket s
       in f (p : ps) (n -1) s'

readPacket :: String -> (Packet, String)
readPacket s = ((binToInt versionStr, ptype), rest)
  where
    (versionStr, s') = splitAt 3 s
    (ptype, rest) = readPacketType s'
    packet = (binToInt versionStr, ptype)

readAllPackets :: String -> ([Packet], String)
readAllPackets s = f [] s
  where
    f ps [] = (reverse ps, [])
    f ps s =
      let (p, s') = readPacket s
       in f (p : ps) s'

readPacketType :: String -> (PacketType, String)
readPacketType ('1' : '0' : '0' : s) = readLiteral s
readPacketType ('0' : '0' : '0' : s) = readOperator Sum s
readPacketType ('0' : '0' : '1' : s) = readOperator Prod s
readPacketType ('0' : '1' : '0' : s) = readOperator Min s
readPacketType ('0' : '1' : '1' : s) = readOperator Max s
readPacketType ('1' : '0' : '1' : s) = readOperator Gt s
readPacketType ('1' : '1' : '0' : s) = readOperator Lt s
readPacketType ('1' : '1' : '1' : s) = readOperator Equals s
readPacketType s
  | all (== '0') s = (None, [])
  | otherwise = error $ "Unknown operator " ++ take 3 s

toBits :: Foldable t => t Char -> String
toBits = concatMap hexCharToBits

versionSum :: String -> Integer
versionSum = versionSumP . fst . readPacket . toBits

versionSumP :: Packet -> Integer
versionSumP (v, pt) = v + versionSumPt pt

versionSumPt :: PacketType -> Integer
versionSumPt (Sum ot) = versionSumOt ot
versionSumPt (Prod ot) = versionSumOt ot
versionSumPt (Min ot) = versionSumOt ot
versionSumPt (Max ot) = versionSumOt ot
versionSumPt (Gt ot) = versionSumOt ot
versionSumPt (Lt ot) = versionSumOt ot
versionSumPt (Equals ot) = versionSumOt ot
versionSumPt (Literal _) = 0
versionSumPt None = 0

versionSumOt :: OpData -> Integer
versionSumOt (A ps) = sum $ map versionSumP ps
versionSumOt (B ps) = sum $ map versionSumP ps