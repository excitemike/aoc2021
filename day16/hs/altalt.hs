import Data.Bits
import Data.Char (digitToInt)
import Data.List (foldl')
import Text.Printf (printf)

type BitData = (Integer, Int)

evalAllPackets :: BitData -> ([Integer], BitData)
evalAllPackets bitData = f [] bitData
  where
    f :: [Integer] -> BitData -> ([Integer], BitData)
    f xs (0, _) = (reverse xs, (0, 0))
    f xs bitData =
      let (x, s') = evalPacket bitData
       in f (x : xs) s'

evalHex :: String -> Integer
evalHex s = fst $ evalPacket (bits, len)
  where
    bitsStr = reverse $ concatMap (printf "%04b" . digitToInt) s
    bits = foldl' (\a b -> 2 * a + fromIntegral (digitToInt b)) 0 bitsStr
    len = length bitsStr

evalList :: Integer -> BitData -> ([Integer], BitData)
evalList mode bits
  | mode == 0 =
    let (revLen, bits'') = splitBits 15 bits'
        len = fromIntegral $ fst $ reverseBits revLen
        (subBits, bits''') = splitBits len bits''
     in (fst $ evalAllPackets subBits, bits''')
  | otherwise =
    let (revLen, bits'') = splitBits 11 bits'
        len = fst $ reverseBits revLen
     in evalNPackets len bits''
  where
    (modeBits, bits') = splitBits 1 bits
    mode = fst modeBits

evalLiteral :: BitData -> (Integer, BitData)
evalLiteral = f 0
  where
    f :: Integer -> BitData -> (Integer, BitData)
    f x (_, 0) = (x, (0, 0))
    f x bits =
      case mode of
        1 -> f x' rest
        _ -> (x', rest)
      where
        (modeBits, bits') = splitBits 1 bits
        mode = fst modeBits
        (revByte, rest) = splitBits 4 bits'
        byte = reverseBits revByte
        x' = 16 * x + fromIntegral (fst byte)

evalNPackets :: Integer -> BitData -> ([Integer], BitData)
evalNPackets n bits = f [] n bits
  where
    f xs 0 bits = (reverse xs, bits)
    f xs n bits =
      let (x, bits') = evalPacket bits
       in f (x : xs) (n -1) bits'

evalOp :: ([Integer] -> Integer) -> BitData -> (Integer, BitData)
evalOp f bits = (f children, rest)
  where
    (modeBit, bits') = splitBits 1 bits
    mode = fst modeBit
    (children, rest) = evalList mode bits

evalPacket :: BitData -> (Integer, BitData)
evalPacket = evalPacketType . snd . splitBits 3

evalPacketType :: BitData -> (Integer, BitData)
evalPacketType bits =
  case typeCode of
    0 -> evalOp sum rest
    1 -> evalOp product rest
    2 -> evalOp minimum rest
    3 -> evalOp maximum rest
    4 -> evalLiteral rest
    5 -> evalOp gt rest
    6 -> evalOp lt rest
    7 -> evalOp equals rest
    _ -> error $ "Unknown operator " ++ show typeCode
  where
    (revTypeCode, rest) = splitBits 3 bits
    typeCode = fst $ reverseBits revTypeCode
    gt [a, b] = if a > b then 1 else 0
    gt _ = error "gt"
    lt [a, b] = if a < b then 1 else 0
    lt _ = error "lt"
    equals [a, b] = if a == b then 1 else 0
    equals _ = error "lt"

main :: IO ()
main = do
  tests
  input <- readFile "input"
  putStr "Only Part 2: "
  print $ evalHex input

reverseBits :: BitData -> BitData
reverseBits (bits, len) = (reversedBits, len)
  where
    n = fromIntegral len
    mask = (1 `shiftL` fromIntegral n) - (1 :: Integer)
    masked = fromIntegral bits .&. fromIntegral mask
    reversedBits = f n 0 masked
    f 0 outBits _ = outBits
    f n outBits inBits = f (n -1) out' rest
      where
        (bit, rest) = (inBits .&. 1, inBits `shiftR` 1)
        out' = (outBits `shiftL` 1) + bit

-- pop some bits off
splitBits :: Int -> BitData -> (BitData, BitData)
splitBits nBits (bits, len) = (lowBits, rest)
  where
    mask = ((1 :: Integer) `shiftL` nBits) - 1
    lowBits = (mask .&. fromIntegral bits, nBits)
    rest = (bits `shiftR` fromIntegral nBits, len - nBits)

tests :: IO ()
tests = do
  expect (0x53, 8) $ reverseBits (0xca, 8)
  expect (0xca, 8) $ reverseBits (0x53, 8)
  expect 2021 $ evalHex "D2FE28"
  expect 1 $ evalHex "38006F45291200"
  expect 3 $ evalHex "EE00D40C823060"
  expect 15 $ evalHex "8A004A801A8002F478"
  expect 3 $ evalHex "C200B40A82"
  expect 54 $ evalHex "04005AC33890"
  expect 7 $ evalHex "880086C3E88112"
  expect 9 $ evalHex "CE00C43D881120"
  expect 1 $ evalHex "D8005AC2A8F0"
  expect 0 $ evalHex "F600BC2D8F"
  expect 0 $ evalHex "9C005AC2F8F0"
  expect 1 $ evalHex "9C0141080250320F1802104A08"
  where
    expect expected got = if expected == got then return () else error $ "expected " ++ show expected ++ ", got " ++ show got
