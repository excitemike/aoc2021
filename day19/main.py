from collections import Counter
import functools
from itertools import combinations
import os
import sys

def point_add(a,b):
    return tuple((a+b for a,b in zip(a,b)))

def parse_block(block):
    return

fname = sys.argv[1] if len(sys.argv) > 1 else "input"

with open(fname) as f:
    input = f.read()

data = [
    [ tuple((int(x)for x in line.split(',')))
      for line in block.splitlines()[1:]]
    for block in input.split('\n\n')]

def compose2(f,g):
    return lambda x: f(g(x))

def compose(*fs):
    return functools.reduce(compose2, fs)

def ident(p):
    return p
def inv(p):
    (x,y,z) = p
    return (-x,-y,-z)
def swap01(p):
    (x,y,z) = p
    return (y,x,z)
def swap02(p):
    (x,y,z) = p
    return (z,y,x)
def swap12(p):
    (x,y,z) = p
    return (x,z,y)
def inv1(p):
    (x,y,z) = p
    return (x,-y,z)
def inv2(p):
    (x,y,z) = p
    return (x,y,-z)

orientation_fs = [
    compose (f4,f3,f2,f1)
    for f1 in [ident, compose(inv2,swap01), compose(inv1,swap02)]
    for f2 in [ident, compose(inv, swap12)]
    for f3 in [ident, compose(inv2,swap12)]
    for f4 in [ident, compose(inv2,inv1)]
]

def scanner_set(scanner):
    return [
        list(map(f, scanner))
        for f in orientation_fs
    ]

scanners = list(map(scanner_set, data))

scanner_pair_data = {i:dict() for i,_ in enumerate(scanners)}
unfinished_scanners = set(scanner_pair_data.keys())
del scanner_pair_data[0]
unfinished_scanners.remove(0)
finished_scanners = [scanners[0][0]]
scanner_locs = []

def find_matching_orientation(orientations, to_match):
    for ori, ori_beacons in enumerate(orientations):
        differences = Counter([
            (ax-bx, ay-by, az-bz)
            for (ax,ay,az) in to_match
            for (bx,by,bz) in ori_beacons
        ])
        for delta, count in differences.items():
            if count >= 12:
                return (ori, delta)
    return None

while unfinished_scanners:
    did_something = False
    for i in unfinished_scanners:
        orientations = scanners[i]
        for rel_beacons in finished_scanners:
            ori_off = find_matching_orientation(orientations, rel_beacons)
            if ori_off is None:
                continue
            ori_index, offset = ori_off
            final = [point_add(beacon, offset) for beacon in orientations[ori_index]]
            did_something = True
            unfinished_scanners.remove(i)
            finished_scanners.append(final)
            scanner_locs.append(offset)
            break
        if did_something:
            break
    if not did_something:
        raise AssertionError("failed to resolve anything")

n_beacons = len({beacon for scanner in finished_scanners for beacon in scanner})
print(f"Part 1: {n_beacons}")

biggest_distance = max((sum((abs(a-b) for a,b in zip(p0,p1))) for p0,p1 in combinations(scanner_locs, 2) ))
print(f"Part 2: {biggest_distance}")
