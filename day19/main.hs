{-# LANGUAGE ScopedTypeVariables #-}
import Data.List.Split (splitOn)
import qualified Data.Set as Set
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromJust, mapMaybe )

type Point = (Int,Int,Int)
type Beacon = Point
type ScannerOri = [Beacon]
type ScannerLoc = Point
type Scanner = [ScannerOri]

oriFs :: [Beacon -> Beacon]
oriFs = [ \(x,y,z) -> ( x, y, z)
        , \(x,y,z) -> ( x,-y,-z)
        , \(x,y,z) -> ( x, z,-y)
        , \(x,y,z) -> ( x,-z, y)

        , \(x,y,z) -> (-x, y,-z)
        , \(x,y,z) -> (-x,-y, z)
        , \(x,y,z) -> (-x, z, y)
        , \(x,y,z) -> (-x,-z,-y)

        , \(x,y,z) -> ( y, x,-z)
        , \(x,y,z) -> ( y,-x, z)
        , \(x,y,z) -> ( y, z, x)
        , \(x,y,z) -> ( y,-z,-x)

        , \(x,y,z) -> (-y, x, z)
        , \(x,y,z) -> (-y,-x,-z)
        , \(x,y,z) -> (-y, z,-x)
        , \(x,y,z) -> (-y,-z, x)

        , \(x,y,z) -> ( z, x, y)
        , \(x,y,z) -> ( z,-x,-y)
        , \(x,y,z) -> ( z, y,-x)
        , \(x,y,z) -> ( z,-y, x)

        , \(x,y,z) -> (-z, x,-y)
        , \(x,y,z) -> (-z,-x, y)
        , \(x,y,z) -> (-z, y, x)
        , \(x,y,z) -> (-z,-y,-x)
        ]

enumerate :: [z] -> [(Int, z)]
enumerate = zip [0..]

frequencies :: (Ord a) => [a] -> Map.Map a Int
frequencies = foldr (\x m -> Map.insertWith (+) x 1 m) Map.empty

genOris:: ScannerOri -> Scanner
genOris x = map (`map` x) oriFs

main = do
    input <- readFile "input"
    let scannerBlocks = splitOn [""] $ lines input
    let scanners = map ((genOris . map (\line->read $ "("++line++")")) . tail) scannerBlocks
    let scannerMap = Map.fromList $ enumerate scanners
    let unfinished = Set.delete 0 $ Set.fromList $ Map.keys scannerMap
    let finished = [head $ head scanners]
    let locs = [(0,0,0)]
    let state = (unfinished, finished, locs)
    let (beacons, locs') = run state scannerMap
    putStr "Part 1:"
    print $ length beacons
    putStr "Part 2:"
    print $ maximum [manhattan a b | (i,a) <- enumerate locs', b <- drop (i+1) locs']

manhattan :: Point -> Point -> Int
manhattan (x0,y0,z0) (x1,y1,z1) = abs (x1-x0) + abs (y1-y0) + abs (z1-z0)

pointAdd :: Point -> Point -> Point
pointAdd (x0,y0,z0) (x1,y1,z1) = (x1+x0, y1+y0, z1+z0)

pointDifference :: Point -> Point -> Point
pointDifference (x0,y0,z0) (x1,y1,z1) = (x1-x0, y1-y0, z1-z0)

run :: (Set.Set Int, [ScannerOri], [ScannerLoc]) -> Map.Map Int Scanner -> (Set.Set Beacon, [ScannerLoc])
run state@(unfinished, finished, locs) scannerMap
  | null unfinished = (mergedFinished, locs)
  | otherwise =
    if null matches
    then error ("got stuck: " ++ show unfinished)
    else run (unfinished', finished', locs') scannerMap
  where
    findMatch :: Int -> Maybe (Int, Int, Point)
    findMatch scannerI = matchSearch 0 allOris finished
      where
        allOris = fromJust $ Map.lookup scannerI scannerMap
        matchSearch :: Int -> Scanner -> [ScannerOri] -> Maybe (Int, Int, Point)
        matchSearch _ [] [] = Nothing
        matchSearch oriI (ori:oris) [] = Nothing
        matchSearch oriI [] (toMatch:finished) = matchSearch 0 allOris finished
        matchSearch oriI (ori:oris) (toMatch:finished) = do
          let differences = [ (scannerI, oriI, pointDifference a b)
                            | a <- ori
                            , b <- toMatch
                            ]
              freqs = frequencies $ map (\(_,_,x)->x) differences
              hasTwelve (_,_,offset) = (>=12) $ fromJust $ Map.lookup offset freqs
              twelves = filter hasTwelve differences
          if null twelves
          then matchSearch (oriI+1) oris (toMatch:finished)
          else Just $ head twelves
    finished' = finished ++ transformed
    indices = map (\(x,_,_)->x) matches
    locs' = locs ++ offsets
    matches = mapMaybe findMatch (Set.toList unfinished)
    mergedFinished = Set.fromList $ concat finished
    offsets = map (\(_,_,x)->x) matches
    transformed = map transformScanner matches
    transformScanner :: (Int, Int, Point) -> ScannerOri
    transformScanner (scannerI, oriI, offset) = map (pointAdd offset) ori
      where
        scanner = fromJust $ Map.lookup scannerI scannerMap
        ori = scanner !! oriI
    unfinished' = Set.difference unfinished $ Set.fromList indices
