import Data.Char (digitToInt)
import Data.List (foldl')
import qualified Data.Map.Strict as Map
import Data.Maybe (fromJust, fromMaybe, isJust)
import qualified Data.Set as Set

type RiskF = Point -> Maybe Int

type RiskMap = Map.Map Point Int

type Point = (Int, Int)

-- put Int first so that Set.minView is like popping off a priority queue
type PointToVisit = (Int, Point)

type ToVisit = Set.Set PointToVisit

type Visited = Set.Set Point

enumerate :: [b] -> [(Int, b)]
enumerate = zip [0 ..]

dijk :: RiskF -> Point -> Int
dijk riskF target = recurse Set.empty (Set.singleton (0, (0, 0)))
  where
    recurse :: Visited -> ToVisit -> Int
    recurse visited queue
      | p == target = pathCost
      | p `Set.member` visited = recurse visited qWithoutP
      | otherwise = recurse updatedVisited qWithNeighbors
      where
        ((pathCost, p), qWithoutP) =
          case Set.minView queue of
            Just x -> x
            Nothing -> error "queue empty"
        updatedVisited = Set.insert p visited
        qWithNeighbors = insertIntoQueue $ withPathCosts $ unvisited $ neighbors p
        unvisited = filter (`Set.notMember` updatedVisited)
        insertIntoQueue = foldl' (flip Set.insert) qWithoutP
        neighbors (x, y) = [(x + 1, y), (x, y + 1), (x -1, y), (x, y -1)]
        withPathCosts ps = [(pathCost + fromJust risk, p) | p <- ps, let risk = riskF p, isJust risk]

main :: IO ()
main = do
  exampleInput <- readFile "exampleinput"
  input <- readFile "input"
  putStrLn $ "Example: " ++ show (run 1 exampleInput)
  putStrLn $ "Part 1:  " ++ show (run 1 input)
  putStrLn $ "Example: " ++ show (run 5 exampleInput)
  putStrLn $ "Part 2:  " ++ show (run 5 input)

run :: Int -> [Char] -> Int
run repeats input = dijk riskF target
  where
    nRows = length rowData
    nCols = length $ head rowData
    target = (repeats * nRows - 1, repeats * nCols - 1)
    rowData = lines input
    grid = map (map digitToInt) rowData
    risks = Map.fromList [((c, r), risk) | (r, row) <- enumerate grid, (c, risk) <- enumerate row]
    riskF (x, y)
      | xPage < 0 = Nothing
      | yPage < 0 = Nothing
      | xPage >= repeats = Nothing
      | yPage >= repeats = Nothing
      | otherwise = riskMath <$> Map.lookup (x', y') risks
      where
        x' = rem x nCols
        y' = rem y nRows
        xPage = quot x nCols
        yPage = quot y nRows
        riskMath x = let x' = x + xPage + yPage in if x' < 10 then x' else 1 + (x' `rem` 10)