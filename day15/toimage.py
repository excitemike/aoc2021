from PIL import Image

max_cost = 3045


def truncate(x):
    return int(min(255, max(0, x)))


def color(cost):
    if cost == -1:
        return [0, 0, 0, 0]
    t = (cost / 3045) * 100 % 1.0
    return [
        truncate(round(34.61 + t * (1172.33 - t * (10793.56 - t *
                                                    (33300.12 - t * (38394.49 - t * 14825.05)))))),
        truncate(round(23.31 + t * (557.33 + t * (1225.33 -
                                                    t * (3574.96 - t * (1073.77 + t * 707.56)))))),
        truncate(round(27.2 + t * (3211.1 - t * (15327.97 -
                                                    t * (27814 - t * (22569.18 - t * 6838.66))))))
    ]
    # t = 255 * cost // 3045
    # return [
    #     (100 * t) % 256,
    #     (100 * t) % 256,
    #     (100 * t) % 256,
    # ]


bytes = bytes((
    x for line in open('debug')
    for cost in line.split()
    for x in color(int(cost))))
img = Image.frombytes('RGB', (500, 500), bytes)
img.save("vis.png")
