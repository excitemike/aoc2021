import Data.Either
import Data.List
import Data.Maybe

isOpener :: Char -> Bool
isOpener c = isJust $ find (== c) "([{<"

closer :: Char -> Char
closer c = maybe (error $ "No closer for " ++ [c]) (")]}>" !!) (elemIndex c "([{<")

isCloserOf :: Char -> Char -> Bool
c `isCloserOf` o = c == closer o

p1ScoreC :: Num p => Char -> p
p1ScoreC c = maybe 0 ([3, 57, 1197, 25137] !!) $ elemIndex c ")]}>"

validate :: String -> String -> Either String Char
validate [] [] = Right '0'
validate stack [] = Left stack
validate [] (x : rest)
  | isOpener x = validate [x] rest
  | otherwise = Right x
validate (x : stack) (y : rest)
  | y `isCloserOf` x = validate stack rest
  | isOpener y = validate (y : x : stack) rest
  | otherwise = Right y

p2ScoreC :: Char -> Int
p2ScoreC c = maybe 0 ([1 .. 5] !!) $ elemIndex c ")]}>"

stackScore :: Int -> [Char] -> Int
stackScore = foldl (\score first -> (+) (score * 5) $ p2ScoreC $ closer first)

middle :: [a] -> a
middle xs = (!!) xs $ (`div` 2) $ length xs

main :: IO ()
main = do
  input <- readFile "input"
  let validateResults = map (validate []) $ lines input
  putStr "Part 1: "
  print $ sum $ map p1ScoreC $ rights validateResults
  putStr "Part 2: "
  print $ middle $ sort $ map (stackScore 0) $ lefts validateResults