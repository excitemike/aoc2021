openers = "([{<"
closers = ")]}>"
corruption_values = (3, 57, 1197, 25137)
incomplete_values = (1, 2, 3, 4)
corrupt_tbl = dict(zip(closers, corruption_values))
incomplete_tbl = dict(zip(openers, incomplete_values))
closer_of = dict(zip(openers, closers))
def middle(xs): return xs[len(xs) >> 1]


p1 = 0
incomplete_scores = []
for line in open("input"):
    stack = []
    for c in line.strip():
        if c in openers:
            stack.append(c)
        elif c == closer_of[stack[-1]]:
            stack.pop()
        else:  # closer with no match on top of stack -> corrupt
            p1 += corrupt_tbl[c]
            break
    else:  # not corrupt
        if stack:  # incomplete
            score = 0
            while stack:
                score = 5*score + incomplete_tbl[stack.pop()]
            incomplete_scores.append(score)
print(p1, middle(sorted(incomplete_scores)))
