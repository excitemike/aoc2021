from copy import deepcopy


def mark(cards, number_to_mark):
    for card in cards:
        for row in card:
            for i, (space_number, _) in enumerate(row):
                if space_number == number_to_mark:
                    row[i] = (space_number, True)


def card_done(card):
    return (any((all((marked for (n, marked) in row))
                 for row in card))
            or
            any((all((row[col][1] for row in card))
                 for col in range(5)))
            )


def score(card):
    return sum((number
                for row in card
                for (number, marked) in row
                if not marked))


with open("input", "r") as f:
    lines = list(f.read().strip().splitlines())
draws = list(map(int, lines[0].split(',')))
cards = []
cur_card = []
for line in lines[1:]:
    if len(line.strip()) > 0:
        row = list(map(lambda x: (int(x), False), line.split()))
        cur_card.append(row)
        if len(cur_card) == 5:
            cards.append(cur_card)
            cur_card = []
p2_cards = deepcopy(cards)
for draw in draws:
    mark(cards, draw)
    winners = list(filter(card_done, cards))
    if winners:
        print("Part 1:", draw * score(winners[0]))
        break

for draw in draws:
    mark(p2_cards, draw)
    winners, losers = [], []
    for card in p2_cards:
        (losers, winners)[card_done(card)].append(card)
    if losers:
        p2_cards = losers
    else:
        print("Part 2:", draw * score(winners[0]))
        break
