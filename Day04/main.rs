#[derive(Clone, Debug)]
struct BingoSpace {
    number: u8,
    marked: bool,
}
impl From<u8> for BingoSpace {
    fn from(x:u8) -> Self {
        BingoSpace {
            number: x,
            marked: false
        }
    }
}
trait BingoCard {
    fn sum_of_marked(&self) -> usize;
}
impl BingoCard for Vec<Vec<BingoSpace>> {
    fn sum_of_marked(&self) -> usize {
        self.iter().map(|row|row.iter().map(|x|x.number as usize).sum::<usize>()).sum::<usize>()
    }
}
trait Bingo {
    fn done(&self) -> bool;
    fn has_number(&self, x:u8) -> bool;
    fn mark(&mut self, x:u8);
    fn score(&self) -> usize;
}
impl Bingo for Vec<Vec<Vec<BingoSpace>>> {
    fn done(&self) -> bool {
        self.iter().any(|card|card.done())
    }
    fn has_number(&self, x:u8) -> bool {
        self.iter().any(|card|card.has_number(x))
    }
    fn mark(&mut self, x:u8) {
        for card in self.iter_mut() {
            card.mark(x)
        }
    }
    fn score(&self) -> usize {
        for card in self.iter() {
            if card.done() {
                return card.score();
            }
        }
        return 0;
    }
}
impl Bingo for Vec<Vec<BingoSpace>> {
    fn done(&self) -> bool {
        self.iter().any(|row|row.done()) || (0..5).any(|i|self.iter().map(|row|&row[i]).all(|x|x.marked))
    }
    fn has_number(&self, x:u8) -> bool {
        self.iter().any(|row|row.has_number(x))
    }
    fn mark(&mut self, x:u8) {
        for row in self.iter_mut() {
            row.mark(x)
        }
    }
    fn score(&self) -> usize {
        self.iter().map(|row|row.score()).sum()
    }
}
impl Bingo for Vec<BingoSpace> {
    fn done(&self) -> bool {
        self.iter().all(|x|x.marked)
    }
    fn has_number(&self, x:u8) -> bool {
        self.iter().any(|space|space.has_number(x))
    }
    fn mark(&mut self, x:u8) {
        for space in self.iter_mut() {
            space.mark(x)
        }
    }
    fn score(&self) -> usize {
        self.iter()
            .map(|space| { if space.marked { 0 } else { space.number as usize } })
            .sum()
    }
}
impl Bingo for BingoSpace {
    fn done(&self) -> bool {
        self.marked
    }
    fn has_number(&self, x:u8) -> bool {
        self.number == x
    }
    fn mark(&mut self, x:u8) {
        if self.number == x {
            self.marked = true;
        }
    }
    fn score(&self) -> usize {
        if self.marked { 0 } else { self.number as usize }
    }
}
trait AllDone {
    fn all_done(&self) -> bool;
}
impl AllDone for Vec<Vec<Vec<BingoSpace>>> {
    fn all_done(&self) -> bool {
        self.iter().all(|card|card.done())
    }
}
fn parse_card(s:&str) -> Vec<Vec<BingoSpace>> {
    s.trim_end()
        .split("\r\n")
        .map(parse_row)
        .collect()
}
fn parse_row(s:&str) -> Vec<BingoSpace> {
    s.split(' ')
        .filter(|s|!s.is_empty())
        .map(|s|s.parse::<u8>().unwrap().into())
        .collect()
}
fn main() {
    let input = std::fs::read_to_string("input").unwrap();
    let blocks:Vec<_> = input.split("\r\n\r\n").collect();
    let draws:Vec<_> = blocks[0]
        .trim_end()
        .split(',')
        .map(|x|x.parse::<u8>().unwrap())
        .collect();
    let mut cards:Vec<_> = blocks[1..]
        .iter()
        .map(|s|parse_card(*s))
        .collect();
    let mut p2_cards = cards.clone();
    for &draw in draws.iter() {
        cards.mark(draw);
        if cards.done() {
            let card_score = cards.score();
            let score = card_score * draw as usize;
            println!("Part 1: {}", score);
            break;
        }
    }
    let mut done_cards:Vec<_> = cards.iter().map(|_|false).collect();
    for &draw in draws.iter() {
        p2_cards.mark(draw);
        let just_finished_cards:Vec<_> = p2_cards.iter()
            .enumerate()
            .filter(|x|(!done_cards[x.0]) && x.1.done() && x.1.has_number(draw))
            .collect();
        if p2_cards.all_done() {
            assert!(just_finished_cards.len() == 1);
            let card = just_finished_cards[0];
            let score = card.1.score() * draw as usize;
            println!("Part 2: {}", score);
            break;
        }
        for (i, _) in just_finished_cards {
            done_cards[i] = true;
        }
    }
}