{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
import qualified Data.Char as Char
import Data.List (transpose)
import Debug.Trace (trace)
split :: Char -> String -> [String]
split delimiter s = case dropWhile pred s of
    "" -> []
    s' -> let (fst,rest) = break pred s'
          in fst : split delimiter rest
  where
    pred = (==delimiter)

type Cards = [Card]
type Card = [CardRow]
type CardRow = [BingoSpace]
type BingoSpace = (Integer, Bool)
class Bingo a where
    mark :: Integer -> a -> a
instance Bingo Cards where
    mark number self = map (mark number) self
instance Bingo Card where
    mark number self = map (mark number) self
instance Bingo CardRow where
    mark number self = map (mark number) self
instance Bingo BingoSpace where
    mark numberToMark self =
        let (spaceNumber, marked) = self
        in if spaceNumber==numberToMark then (spaceNumber, True) else self
rowComplete = all snd
cardIsDone card = any (any rowComplete) [card, transpose card]
doneCards = filter cardIsDone
score :: Card -> Integer
score = sum . map rowScore
  where
      rowScore = sum . map spaceScore
      spaceScore (n,marked) = if marked then 0 else n

runP1 :: [Integer] -> Cards -> Integer
runP1 (draw:remainingDraws) cards =
    let cards' = mark draw cards
    in case doneCards cards' of
        [] -> runP1 remainingDraws cards'
        (winner:_) -> draw * score winner

runP2 :: [Integer] -> Cards -> Integer
runP2 (draw:remainingDraws) cards =
    let cards' = mark draw cards
    in case doneCards cards' of
        [] -> runP2 remainingDraws cards'
        winners -> if length winners == length cards'
                   then draw * score (head winners)
                   else let losers = filter (`notElem` winners) cards'
                        in runP2 remainingDraws losers
main :: IO ()
main = do
    input_lines <- lines <$> readFile "input"
    let (draws_row:_:rest) = input_lines
        draws = map (\x -> read x :: Integer) $ split ',' draws_row
        cards = readCards rest
        p1 = runP1 draws cards
        p2 = runP2 draws cards
    putStrLn $ "Part 1: " ++ show p1
    putStrLn $ "Part 2: " ++ show p2
  where
    readCards input_lines = case dropWhile (all Char.isSpace) input_lines of
        [] -> []
        input_lines' -> let card = readCard input_lines'
                            rest = drop 5 input_lines'
                        in card : readCards rest
    readCard input_lines = map readRow $ take 5 input_lines
    readRow line = map readSpace $ words line
    readSpace x = (read x :: Integer, False)