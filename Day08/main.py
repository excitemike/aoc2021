with open("input", "r") as f:
    data = [ [[''.join(sorted(x)) for x in half.split()] for half in row.strip().split(' | ')]
             for row in f.read().strip().splitlines()
           ]
p1 = [ word
       for row in data
       for word in row[1]
       if len(word) in [2, 3, 4, 7]]
print(f"Part 1: {len(p1)}")

digit_possibilities_by_length = [None, None, {1,}, {7,}, {4,}, {2,3,5}, {0,6,9}, {8,}]
digits_to_segments = 'abcefg cf acdeg acdfg bcdf abdfg abdefg acf abcdefg abcdfg'.split()
wire_possibilities_by_length = [
    set().union(*(set(digits_to_segments[digit]) for digit in digits)) if digits else None
    for digits in digit_possibilities_by_length
]
def enumerate_wire_maps(digit_codes):
    def f(wires, wire_map, wire_possibilities):
        if wires:
            wire = wires[0]
            wires = wires[1:]
            for segment in wire_possibilities[wire]:
                if segment not in wire_map.values():
                    wire_map[wire] = segment
                    yield from f(wires, wire_map, wire_possibilities)
                    del wire_map[wire]
        else:
            yield wire_map
    return f('abcdefg', dict(), get_wire_possibilities(digit_codes))
def validate_wire_map(wire_map, digit_codes):
    for code in digit_codes:
        segments = ''.join(sorted((wire_map[c] for c in code)))
        if segments not in digits_to_segments:
            return False
    return True
def apply_wire_map(wire_map, display):
    n = 0
    for display_item in display:
        s = ''.join(sorted((wire_map[c] for c in display_item)))
        digit_value = digits_to_segments.index(s)
        n = n * 10 + digit_value
    return n
def get_wire_possibilities(digit_codes):
    wire_possibilities = {c:set('abcdefg') for c in 'abcdefg'}
    for digit_code in digit_codes:
        ps = wire_possibilities_by_length[len(digit_code)]
        for wire in digit_code:
            wire_possibilities[wire].intersection_update(ps)
    return wire_possibilities
def solve(digit_codes, display):
    for wire_map in enumerate_wire_maps(digit_codes):
        if validate_wire_map(wire_map, digit_codes):
            return apply_wire_map(wire_map, display)
print(f"Part 2: {sum((solve(digit_codes, display) for digit_codes, display in data))}")