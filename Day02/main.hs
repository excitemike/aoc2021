import GHC.List (foldl')

data State = State
  { horizontal :: Integer,
    depth :: Integer,
    aim :: Integer
  }

main = do
  instructions <- parsedInput
  part 1 instructions
  part 2 instructions

part n input = putStrLn $ "Part " ++ show n ++ ": " ++ toResult (foldl' (doInstruction n) initialState input)

toResult state = show (horizontal state * depth state)

initialState = State {horizontal = 0, depth = 0, aim = 0}

doInstruction 1 state ("forward", amount) = state {horizontal = horizontal state + amount}
doInstruction 1 state ("down", amount) = state {depth = depth state + amount}
doInstruction 1 state ("up", amount) = state {depth = depth state - amount}
doInstruction 2 state ("forward", amount) =
  state
    { horizontal = horizontal state + amount,
      depth = depth state + aim state * amount
    }
doInstruction 2 state ("down", amount) = state {aim = aim state + amount}
doInstruction 2 state ("up", amount) = state {aim = aim state - amount}
doInstruction _ _ _ = error "unhandled case"

parsedInput = parse <$> readFile "input"

parse input = map parseInstruction $ lines input

parseInstruction line = let [a, b] = words line in (a, read b)