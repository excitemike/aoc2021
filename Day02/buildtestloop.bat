@echo off
setlocal
set BASENAME=main
set SRC=%BASENAME%.hs
set EXE=%BASENAME%_hs.exe

:start
cls
if not exist %EXE% goto build
echo Deleting %EXE%...
del %EXE%

:build
echo Building %EXE%...
ghc %SRC% -O -o %EXE%
if not exist %EXE% goto error

:run
echo Running %EXE%...
%EXE%

pause
goto start

:error
echo ERROR
pause
goto start