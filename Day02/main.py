class State:
    def __init__(self):
        self.aim = 0
        self.depth = 0
        self.horizontal = 0
    def runP1(self, instructions):
        for (command, amount) in instructions:
            if command == "forward":
                self.horizontal += amount
            if command == "down":
                self.depth += amount
            if command == "up":
                self.depth -= amount
        return self
    def runP2(self, instructions):
        for (command, amount) in instructions:
            if command == "forward":
                self.horizontal += amount
                self.depth += self.aim * amount
            if command == "down":
                self.aim += amount
            if command == "up":
                self.aim -= amount
        return self
    def toResult(self):
        return self.horizontal * self.depth
def parseInstruction(line):
    (command, amount) = line.split()
    return (command, int(amount))
with open("input", "r") as f:
    instructions = list(map(parseInstruction, f.read().strip().splitlines()))
    print("Part 1:", State().runP1(instructions).toResult())
    print("Part 2:", State().runP2(instructions).toResult())